package scripts.fluffeesRedDragonKiller.gui;

public class FXML1 {

    public static String fxmlContents = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n" +
            "\n" +
            "<?import scripts.fluffeesapi.scripting.javafx.jfoenix.controls.JFXButton?>\n" +
            "<?import scripts.fluffeesapi.scripting.javafx.glyphs.fontawesome.FontAwesomeIcon?>\n" +
            "<?import javafx.geometry.Insets?>\n" +
            "<?import javafx.scene.control.ComboBox?>\n" +
            "<?import javafx.scene.control.Label?>\n" +
            "<?import javafx.scene.control.Spinner?>\n" +
            "<?import javafx.scene.control.TextField?>\n" +
            "<?import javafx.scene.layout.AnchorPane?>\n" +
            "<?import javafx.scene.layout.HBox?>\n" +
            "<?import javafx.scene.layout.Pane?>\n" +
            "<?import javafx.scene.layout.VBox?>\n" +
            "<?import javafx.scene.text.Font?>\n" +
            "\n" +
            "<AnchorPane maxHeight=\"-Infinity\" maxWidth=\"-Infinity\" minHeight=\"-Infinity\" minWidth=\"-Infinity\" prefHeight=\"640.0\" prefWidth=\"1049.0\" stylesheets=\"@fluffees-red-dragon-killer.css\" xmlns=\"http://javafx.com/javafx/8.0.141\" xmlns:fx=\"http://javafx.com/fxml/1\" fx:controller=\"scripts.fluffeesRedDragonKiller.gui.GUIController\">\n" +
            "   <children>\n" +
            "      <VBox prefHeight=\"640.0\" prefWidth=\"290.0\" styleClass=\"sidebar\">\n" +
            "         <children>\n" +
            "            <HBox prefHeight=\"100.0\" prefWidth=\"200.0\" style=\"-fx-background-color: #963131;\">\n" +
            "               <children>\n" +
            "                  <Label prefHeight=\"102.0\" prefWidth=\"289.0\" text=\"Fluffee's Red Dragon Killer\" textAlignment=\"CENTER\" textFill=\"WHITE\" wrapText=\"true\">\n" +
            "                     <font>\n" +
            "                        <Font name=\"Roboto Regular\" size=\"33.0\" />\n" +
            "                     </font>\n" +
            "                  </Label>\n" +
            "               </children>\n" +
            "            </HBox>\n" +
            "            <HBox fx:id=\"sidebarMainSettings\" onMouseClicked=\"#sidebarMainSettingsClicked\" prefHeight=\"40.0\" prefWidth=\"276.0\" styleClass=\"sidebar-btns\">\n" +
            "               <children>\n" +
            "                  <FontAwesomeIcon fill=\"WHITE\" iconName=\"GEARS\" size=\"25\">\n" +
            "                     <HBox.margin>\n" +
            "                        <Insets left=\"10.0\" top=\"7.0\" />\n" +
            "                     </HBox.margin>\n" +
            "                  </FontAwesomeIcon>\n" +
            "                  <Label alignment=\"CENTER\" contentDisplay=\"RIGHT\" text=\"Main Settings\" textAlignment=\"CENTER\" textFill=\"WHITE\">\n" +
            "                     <font>\n" +
            "                        <Font size=\"16.0\" />\n" +
            "                     </font>\n" +
            "                     <HBox.margin>\n" +
            "                        <Insets left=\"20.0\" top=\"10.0\" />\n" +
            "                     </HBox.margin>\n" +
            "                  </Label>\n" +
            "               </children>\n" +
            "            </HBox>\n" +
            "            <HBox fx:id=\"sidebarEquipment\" layoutX=\"10.0\" layoutY=\"110.0\" onMouseClicked=\"#sidebarEquipmentClicked\" prefHeight=\"40.0\" prefWidth=\"276.0\" styleClass=\"sidebar-btns\">\n" +
            "               <children>\n" +
            "                  <FontAwesomeIcon fill=\"WHITE\" iconName=\"SHIELD\" size=\"25\">\n" +
            "                     <HBox.margin>\n" +
            "                        <Insets left=\"10.0\" top=\"7.0\" />\n" +
            "                     </HBox.margin>\n" +
            "                  </FontAwesomeIcon>\n" +
            "                  <Label alignment=\"CENTER\" contentDisplay=\"RIGHT\" text=\"Equipment\" textAlignment=\"CENTER\" textFill=\"WHITE\">\n" +
            "                     <font>\n" +
            "                        <Font size=\"16.0\" />\n" +
            "                     </font>\n" +
            "                     <HBox.margin>\n" +
            "                        <Insets left=\"30.0\" top=\"10.0\" />\n" +
            "                     </HBox.margin>\n" +
            "                  </Label>\n" +
            "               </children>\n" +
            "            </HBox>\n" +
            "            <HBox fx:id=\"sidebarMagicSettings\" layoutX=\"10.0\" layoutY=\"270.0\" onMouseClicked=\"#sidebarMagicSettingsClicked\" prefHeight=\"40.0\" prefWidth=\"276.0\" styleClass=\"sidebar-btns\">\n" +
            "               <children>\n" +
            "                  <FontAwesomeIcon fill=\"WHITE\" iconName=\"MAGIC\" size=\"25\">\n" +
            "                     <HBox.margin>\n" +
            "                        <Insets left=\"10.0\" top=\"7.0\" />\n" +
            "                     </HBox.margin>\n" +
            "                  </FontAwesomeIcon>\n" +
            "                  <Label alignment=\"CENTER\" contentDisplay=\"RIGHT\" text=\"Magic Settings\" textAlignment=\"CENTER\" textFill=\"WHITE\">\n" +
            "                     <font>\n" +
            "                        <Font size=\"16.0\" />\n" +
            "                     </font>\n" +
            "                     <HBox.margin>\n" +
            "                        <Insets left=\"20.0\" top=\"10.0\" />\n" +
            "                     </HBox.margin>\n" +
            "                  </Label>\n" +
            "               </children>\n" +
            "            </HBox>\n" +
            "            <HBox layoutX=\"10.0\" layoutY=\"310.0\" prefHeight=\"40.0\" prefWidth=\"276.0\" styleClass=\"sidebar-btns\" translateY=\"340.0\">\n" +
            "               <children>\n" +
            "                  <FontAwesomeIcon fill=\"WHITE\" iconName=\"GROUP\" size=\"19\" textAlignment=\"CENTER\" wrappingWidth=\"14.0\">\n" +
            "                     <HBox.margin>\n" +
            "                        <Insets left=\"10.0\" top=\"10.0\" />\n" +
            "                     </HBox.margin></FontAwesomeIcon>\n" +
            "                  <Label alignment=\"CENTER\" contentDisplay=\"RIGHT\" text=\"Your Accounts Online: 0\" textAlignment=\"CENTER\" textFill=\"WHITE\">\n" +
            "                     <font>\n" +
            "                        <Font size=\"16.0\" />\n" +
            "                     </font>\n" +
            "                     <HBox.margin>\n" +
            "                        <Insets left=\"20.0\" top=\"10.0\" />\n" +
            "                     </HBox.margin>\n" +
            "                  </Label>\n" +
            "               </children>\n" +
            "            </HBox>\n" +
            "            <HBox layoutX=\"10.0\" layoutY=\"360.0\" prefHeight=\"40.0\" prefWidth=\"276.0\" styleClass=\"sidebar-btns\" translateY=\"340.0\">\n" +
            "               <children>\n" +
            "                  <FontAwesomeIcon fill=\"WHITE\" iconName=\"GROUP\" size=\"19\" textAlignment=\"CENTER\" wrappingWidth=\"14.0\">\n" +
            "                     <HBox.margin>\n" +
            "                        <Insets left=\"10.0\" top=\"10.0\" />\n" +
            "                     </HBox.margin></FontAwesomeIcon>\n" +
            "                  <Label alignment=\"CENTER\" contentDisplay=\"RIGHT\" text=\"Total Accounts Online: 0\" textAlignment=\"CENTER\" textFill=\"WHITE\">\n" +
            "                     <font>\n" +
            "                        <Font size=\"16.0\" />\n" +
            "                     </font>\n" +
            "                     <HBox.margin>\n" +
            "                        <Insets left=\"20.0\" top=\"10.0\" />\n" +
            "                     </HBox.margin>\n" +
            "                  </Label>\n" +
            "               </children>\n" +
            "            </HBox>\n" +
            "            <JFXButton fx:id=\"btnStart\" maxHeight=\"-Infinity\" maxWidth=\"-Infinity\" minHeight=\"-Infinity\" minWidth=\"-Infinity\" onMouseClicked=\"#btnStartClicked\" prefHeight=\"70.0\" prefWidth=\"160.0\" style=\"-jfx-disable-visual-focus: true;\" styleClass=\"script-btns\" text=\"Start!\" textFill=\"WHITE\" translateX=\"65.0\" translateY=\"180.0\">\n" +
            "               <graphic>\n" +
            "                  <FontAwesomeIcon fill=\"WHITE\" iconName=\"CHECK_CIRCLE\" size=\"22\" />\n" +
            "               </graphic>\n" +
            "               <font>\n" +
            "                  <Font size=\"22.0\" />\n" +
            "               </font>\n" +
            "            </JFXButton>\n" +
            "            <JFXButton fx:id=\"btnLoad\" maxHeight=\"50.0\" maxWidth=\"-Infinity\" minHeight=\"50.0\" minWidth=\"-Infinity\" onMouseClicked=\"#btnLoadClicked\" prefHeight=\"50.0\" prefWidth=\"120.0\" style=\"-jfx-disable-visual-focus: true;\" styleClass=\"script-btns\" text=\"Load\" textFill=\"WHITE\" translateX=\"16.0\" translateY=\"40.0\">\n" +
            "               <graphic>\n" +
            "                  <FontAwesomeIcon fill=\"WHITE\" iconName=\"CHECK_CIRCLE\" size=\"22\" text=\"\uF058\" />\n" +
            "               </graphic>\n" +
            "               <font>\n" +
            "                  <Font size=\"22.0\" />\n" +
            "               </font>\n" +
            "            </JFXButton>\n" +
            "            <JFXButton fx:id=\"btnSave\" maxHeight=\"50.0\" maxWidth=\"-Infinity\" minHeight=\"50.0\" minWidth=\"-Infinity\" onMouseClicked=\"#btnSaveClicked\" prefHeight=\"50.0\" prefWidth=\"120.0\" style=\"-jfx-disable-visual-focus: true;\" styleClass=\"script-btns\" text=\"Save\" textFill=\"WHITE\" translateX=\"153.0\" translateY=\"-10.0\">\n" +
            "               <graphic>\n" +
            "                  <FontAwesomeIcon fill=\"WHITE\" iconName=\"CHECK_CIRCLE\" size=\"22\" text=\"\uF058\" />\n" +
            "               </graphic>\n" +
            "               <font>\n" +
            "                  <Font size=\"22.0\" />\n" +
            "               </font>\n" +
            "            </JFXButton>\n" +
            "         </children>\n" +
            "      </VBox>\n" +
            "      <Pane fx:id=\"contentArea\" layoutX=\"290.0\" prefHeight=\"640.0\" prefWidth=\"760.0\">\n" +
            "         <children>\n" +
            "            <Pane fx:id=\"paneMagicSettings\" prefHeight=\"640.0\" prefWidth=\"760.0\" style=\"-fx-background-color: #f4f4f4;\">\n" +
            "               <children>\n" +
            "                  <HBox prefHeight=\"100.0\" prefWidth=\"760.0\" style=\"-fx-background-color: #ffffff;\">\n" +
            "                     <children>\n" +
            "                        <Label alignment=\"CENTER\" prefHeight=\"100.0\" prefWidth=\"760.0\" styleClass=\"header\" text=\"Magic Settings\" textAlignment=\"CENTER\" textFill=\"#797d8a\" wrapText=\"true\">\n" +
            "                           <font>\n" +
            "                              <Font size=\"31.0\" />\n" +
            "                           </font>\n" +
            "                           <HBox.margin>\n" +
            "                              <Insets />\n" +
            "                           </HBox.margin>\n" +
            "                           <padding>\n" +
            "                              <Insets left=\"10.0\" />\n" +
            "                           </padding>\n" +
            "                        </Label>\n" +
            "                     </children>\n" +
            "                  </HBox>\n" +
            "                  <Label alignment=\"CENTER\" layoutX=\"66.0\" layoutY=\"125.0\" prefHeight=\"25.0\" prefWidth=\"280.0\" text=\"Staff Settings\" textAlignment=\"CENTER\">\n" +
            "                     <font>\n" +
            "                        <Font size=\"20.0\" />\n" +
            "                     </font>\n" +
            "                  </Label>\n" +
            "                  <Label alignment=\"CENTER\" layoutX=\"66.0\" layoutY=\"165.0\" prefWidth=\"280.0\" text=\"Staff Name\" />\n" +
            "                  <Label alignment=\"CENTER\" layoutX=\"66.0\" layoutY=\"227.0\" prefWidth=\"280.0\" text=\"First Rune Staff Replaces\" />\n" +
            "                  <Label alignment=\"CENTER\" layoutX=\"66.0\" layoutY=\"289.0\" prefWidth=\"280.0\" text=\"Second Rune Staff Replaces\" />\n" +
            "                  <ComboBox fx:id=\"cmbFirstRune\" layoutX=\"66.0\" layoutY=\"248.0\" prefWidth=\"280.0\" />\n" +
            "                  <ComboBox fx:id=\"cmbSecondRune\" layoutX=\"66.0\" layoutY=\"310.0\" prefWidth=\"280.0\" />\n" +
            "                  <Label alignment=\"CENTER\" layoutX=\"66.0\" layoutY=\"351.0\" prefWidth=\"280.0\" text=\"Third Rune Staff Replaces\" />\n" +
            "                  <ComboBox fx:id=\"cmbThirdRune\" layoutX=\"66.0\" layoutY=\"372.0\" prefWidth=\"280.0\" />\n" +
            "                  <TextField fx:id=\"txtStaffName\" layoutX=\"66.0\" layoutY=\"186.0\" maxWidth=\"280.0\" minWidth=\"280.0\" prefWidth=\"280.0\" />\n" +
            "                  <Label alignment=\"CENTER\" layoutX=\"414.0\" layoutY=\"125.0\" prefHeight=\"25.0\" prefWidth=\"280.0\" text=\"Spell Settings\" textAlignment=\"CENTER\">\n" +
            "                     <font>\n" +
            "                        <Font size=\"20.0\" />\n" +
            "                     </font>\n" +
            "                  </Label>\n" +
            "                  <Label alignment=\"CENTER\" layoutX=\"414.0\" layoutY=\"165.0\" prefWidth=\"280.0\" text=\"Spell Name\" />\n" +
            "                  <Label fx:id=\"lblAltFirstRune\" alignment=\"CENTER\" layoutX=\"414.0\" layoutY=\"227.0\" prefWidth=\"280.0\" text=\"Alternative First Rune (x per cast)\" />\n" +
            "                  <Label fx:id=\"lblAltSecondRune\" alignment=\"CENTER\" layoutX=\"414.0\" layoutY=\"289.0\" prefWidth=\"280.0\" text=\"Alternative Second Rune (x per cast)\" />\n" +
            "                  <ComboBox fx:id=\"cmbAlternativeFirstRune\" layoutX=\"414.0\" layoutY=\"248.0\" prefWidth=\"280.0\" />\n" +
            "                  <ComboBox fx:id=\"cmbAlternativeSecondRune\" layoutX=\"414.0\" layoutY=\"310.0\" prefWidth=\"280.0\" />\n" +
            "                  <Label fx:id=\"lblAltThirdRune\" alignment=\"CENTER\" layoutX=\"414.0\" layoutY=\"351.0\" prefWidth=\"280.0\" text=\"Alternative Third Rune (x per cast)\" />\n" +
            "                  <ComboBox fx:id=\"cmbAlternativeThirdRune\" layoutX=\"414.0\" layoutY=\"372.0\" prefWidth=\"280.0\" />\n" +
            "                  <ComboBox fx:id=\"cmbSpellName\" layoutX=\"414.0\" layoutY=\"186.0\" onAction=\"#cmbSpellNameChanged\" prefWidth=\"280.0\" />\n" +
            "               </children>\n" +
            "            </Pane>\n" +
            "            <Pane fx:id=\"paneEquipment\" prefHeight=\"640.0\" prefWidth=\"760.0\" style=\"-fx-background-color: #f4f4f4;\">\n" +
            "               <children>\n" +
            "                  <HBox prefHeight=\"100.0\" prefWidth=\"760.0\" style=\"-fx-background-color: #ffffff;\">\n" +
            "                     <children>\n" +
            "                        <Label alignment=\"CENTER\" prefHeight=\"100.0\" prefWidth=\"760.0\" text=\"Equipment Settings\" textAlignment=\"CENTER\" textFill=\"#797d8a\" wrapText=\"true\">\n" +
            "                           <font>\n" +
            "                              <Font size=\"31.0\" />\n" +
            "                           </font>\n" +
            "                           <HBox.margin>\n" +
            "                              <Insets />\n" +
            "                           </HBox.margin>\n" +
            "                           <padding>\n" +
            "                              <Insets left=\"10.0\" />\n" +
            "                           </padding>\n" +
            "                        </Label>\n" +
            "                     </children>\n" +
            "                  </HBox>\n" +
            "                  <Label alignment=\"CENTER\" layoutX=\"240.0\" layoutY=\"125.0\" prefHeight=\"25.0\" prefWidth=\"280.0\" text=\"Equipment\" textAlignment=\"CENTER\">\n" +
            "                     <font>\n" +
            "                        <Font size=\"20.0\" />\n" +
            "                     </font>\n" +
            "                  </Label>\n" +
            "                  <Label alignment=\"CENTER\" layoutX=\"66.0\" layoutY=\"166.0\" prefWidth=\"280.0\" text=\"Helmet\" />\n" +
            "                  <TextField fx:id=\"txtHelmet\" layoutX=\"66.0\" layoutY=\"187.0\" prefWidth=\"280.0\" />\n" +
            "                  <Label alignment=\"CENTER\" layoutX=\"66.0\" layoutY=\"228.0\" prefWidth=\"280.0\" text=\"Cape\" />\n" +
            "                  <TextField fx:id=\"txtCape\" layoutX=\"66.0\" layoutY=\"249.0\" prefWidth=\"280.0\" />\n" +
            "                  <Label alignment=\"CENTER\" layoutX=\"66.0\" layoutY=\"290.0\" prefWidth=\"280.0\" text=\"Chest\" />\n" +
            "                  <TextField fx:id=\"txtChest\" layoutX=\"66.0\" layoutY=\"311.0\" prefWidth=\"280.0\" />\n" +
            "                  <Label alignment=\"CENTER\" layoutX=\"66.0\" layoutY=\"352.0\" prefWidth=\"280.0\" text=\"Shield\" />\n" +
            "                  <TextField fx:id=\"txtShield\" layoutX=\"66.0\" layoutY=\"373.0\" prefWidth=\"280.0\" />\n" +
            "                  <Label alignment=\"CENTER\" layoutX=\"66.0\" layoutY=\"413.0\" prefWidth=\"280.0\" text=\"Hands\" />\n" +
            "                  <TextField fx:id=\"txtHands\" layoutX=\"66.0\" layoutY=\"434.0\" prefWidth=\"280.0\" />\n" +
            "                  <Label alignment=\"CENTER\" layoutX=\"240.0\" layoutY=\"475.0\" prefWidth=\"280.0\" text=\"Ring\" />\n" +
            "                  <TextField fx:id=\"txtRing\" layoutX=\"240.0\" layoutY=\"496.0\" prefWidth=\"280.0\" />\n" +
            "                  <Label alignment=\"CENTER\" layoutX=\"414.0\" layoutY=\"166.0\" prefWidth=\"280.0\" text=\"Amulet\" />\n" +
            "                  <TextField fx:id=\"txtAmulet\" layoutX=\"414.0\" layoutY=\"187.0\" prefWidth=\"280.0\" />\n" +
            "                  <Label alignment=\"CENTER\" layoutX=\"414.0\" layoutY=\"228.0\" prefWidth=\"280.0\" text=\"Ammo\" />\n" +
            "                  <TextField fx:id=\"txtAmmo\" layoutX=\"414.0\" layoutY=\"249.0\" prefWidth=\"280.0\" />\n" +
            "                  <Label alignment=\"CENTER\" layoutX=\"414.0\" layoutY=\"290.0\" prefWidth=\"280.0\" text=\"Weapon\" />\n" +
            "                  <TextField fx:id=\"txtWeapon\" layoutX=\"414.0\" layoutY=\"311.0\" prefWidth=\"280.0\" />\n" +
            "                  <Label alignment=\"CENTER\" layoutX=\"414.0\" layoutY=\"352.0\" prefWidth=\"280.0\" text=\"Legs\" />\n" +
            "                  <TextField fx:id=\"txtLegs\" layoutX=\"414.0\" layoutY=\"373.0\" prefWidth=\"280.0\" />\n" +
            "                  <Label alignment=\"CENTER\" layoutX=\"414.0\" layoutY=\"413.0\" prefWidth=\"280.0\" text=\"Feet\" />\n" +
            "                  <TextField fx:id=\"txtFeet\" layoutX=\"414.0\" layoutY=\"434.0\" prefWidth=\"280.0\" />\n" +
            "                  <JFXButton fx:id=\"btnGrabEquipment\" layoutX=\"311.0\" layoutY=\"543.0\" onMouseClicked=\"#btnGrabEquipmentClicked\" prefHeight=\"40.0\" prefWidth=\"139.0\" ripplerFill=\"#1e0000\" styleClass=\"inner-btns\" text=\"Grab Equipment\" />\n" +
            "               </children>\n" +
            "            </Pane>\n" +
            "            <Pane fx:id=\"paneMainSettings\" maxHeight=\"640.0\" minHeight=\"640.0\" prefHeight=\"640.0\" prefWidth=\"760.0\" style=\"-fx-background-color: #f4f4f4;\">\n" +
            "               <children>\n" +
            "                  <HBox prefHeight=\"100.0\" prefWidth=\"760.0\" style=\"-fx-background-color: #ffffff;\">\n" +
            "                     <children>\n" +
            "                        <Label alignment=\"CENTER\" prefHeight=\"100.0\" prefWidth=\"760.0\" text=\"Main Settings\" textAlignment=\"CENTER\" textFill=\"#797d8a\" wrapText=\"true\">\n" +
            "                           <font>\n" +
            "                              <Font size=\"31.0\" />\n" +
            "                           </font>\n" +
            "                           <HBox.margin>\n" +
            "                              <Insets />\n" +
            "                           </HBox.margin>\n" +
            "                           <padding>\n" +
            "                              <Insets left=\"10.0\" />\n" +
            "                           </padding>\n" +
            "                        </Label>\n" +
            "                     </children>\n" +
            "                  </HBox>\n" +
            "                  <Label alignment=\"CENTER\" layoutX=\"414.0\" layoutY=\"165.0\" maxWidth=\"280.0\" minWidth=\"280.0\" prefWidth=\"280.0\" text=\"Banking Method\" />\n" +
            "                  <Label alignment=\"CENTER\" layoutX=\"414.0\" layoutY=\"227.0\" maxWidth=\"280.0\" minWidth=\"280.0\" prefWidth=\"280.0\" text=\"Food Per Run\" />\n" +
            "                  <ComboBox fx:id=\"cmbBankMethod\" layoutX=\"414.0\" layoutY=\"186.0\" prefWidth=\"280.0\" />\n" +
            "                  <Label alignment=\"CENTER\" layoutX=\"414.0\" layoutY=\"125.0\" prefHeight=\"25.0\" prefWidth=\"280.0\" text=\"Banking Settings\" textAlignment=\"CENTER\">\n" +
            "                     <font>\n" +
            "                        <Font size=\"20.0\" />\n" +
            "                     </font>\n" +
            "                  </Label>\n" +
            "                  <Label alignment=\"CENTER\" contentDisplay=\"CENTER\" layoutX=\"414.0\" layoutY=\"289.0\" prefHeight=\"16.0\" prefWidth=\"280.0\" text=\"Potions Per Run\" />\n" +
            "                  <Label alignment=\"CENTER\" contentDisplay=\"CENTER\" layoutX=\"414.0\" layoutY=\"351.0\" prefHeight=\"16.0\" prefWidth=\"280.0\" text=\"Ammo Per Run\" />\n" +
            "                  <Spinner fx:id=\"spnPotionsPer\" editable=\"true\" layoutX=\"414.0\" layoutY=\"310.0\" prefHeight=\"26.0\" prefWidth=\"280.0\" min=\"0\" max=\"28\" initialValue=\"0\" />\n" +
            "                  <Spinner fx:id=\"spnAmmoPer\" editable=\"true\" layoutX=\"414.0\" layoutY=\"372.0\" prefHeight=\"26.0\" prefWidth=\"280.0\" min=\"100\" max=\"100000\" initialValue=\"500\"/>\n" +
            "                  <Label alignment=\"CENTER\" layoutX=\"66.0\" layoutY=\"125.0\" prefHeight=\"25.0\" prefWidth=\"280.0\" text=\"General Settings\" textAlignment=\"CENTER\">\n" +
            "                     <font>\n" +
            "                        <Font size=\"20.0\" />\n" +
            "                     </font>\n" +
            "                  </Label>\n" +
            "                  <Label alignment=\"CENTER\" layoutX=\"66.0\" layoutY=\"165.0\" maxWidth=\"280.0\" minWidth=\"280.0\" prefWidth=\"280.0\" text=\"Attacking Method\" />\n" +
            "                  <ComboBox fx:id=\"cmbAttackingMethod\" layoutX=\"66.0\" layoutY=\"186.0\" prefWidth=\"280.0\" />\n" +
            "                  <Label alignment=\"CENTER\" layoutX=\"66.0\" layoutY=\"227.0\" maxWidth=\"280.0\" minWidth=\"280.0\" prefWidth=\"280.0\" text=\"Axe to Use\" />\n" +
            "                  <Label alignment=\"CENTER\" layoutX=\"66.0\" layoutY=\"289.0\" maxWidth=\"280.0\" minWidth=\"280.0\" prefWidth=\"280.0\" text=\"Food to Eat\" />\n" +
            "                  <Label alignment=\"CENTER\" layoutX=\"66.0\" layoutY=\"351.0\" maxWidth=\"280.0\" minWidth=\"280.0\" prefWidth=\"280.0\" text=\"Potion to Use\" />\n" +
            "                  <ComboBox fx:id=\"cmbPotion\" layoutX=\"66.0\" layoutY=\"372.0\" minHeight=\"26.0\" prefHeight=\"26.0\" prefWidth=\"280.0\" />\n" +
            "                  <ComboBox fx:id=\"cmbAxe\" layoutX=\"66.0\" layoutY=\"248.0\" minHeight=\"26.0\" prefHeight=\"26.0\" prefWidth=\"280.0\" />\n" +
            "                  <ComboBox fx:id=\"cmbFood\" layoutX=\"66.0\" layoutY=\"310.0\" minHeight=\"26.0\" prefHeight=\"26.0\" prefWidth=\"280.0\" />\n" +
            "                  <Spinner fx:id=\"spnFoodPer\" editable=\"true\" layoutX=\"414.0\" layoutY=\"248.0\" prefHeight=\"26.0\" prefWidth=\"280.0\" min=\"0\" max=\"28\" initialValue=\"5\"/>\n" +
            "               </children>\n" +
            "            </Pane>\n" +
            "         </children>\n" +
            "      </Pane>\n" +
            "      <FontAwesomeIcon fx:id=\"minimizeButton\" fill=\"#797d8a\" iconName=\"MINUS\" layoutX=\"990.0\" layoutY=\"33.0\" onMouseClicked=\"#minimizeButtonClicked\" size=\"30\" styleClass=\"close-btns\" />\n" +
            "      <FontAwesomeIcon fx:id=\"closeButton\" fill=\"#797d8a\" iconName=\"CLOSE\" layoutX=\"1020.0\" layoutY=\"25.0\" onMouseClicked=\"#closeButtonClicked\" size=\"30\" styleClass=\"close-btns\" />\n" +
            "   </children>\n" +
            "</AnchorPane>";

}
