package scripts.fluffeesRedDragonKiller.gui;

import com.allatori.annotations.DoNotRename;
import javafx.collections.FXCollections;
import javafx.fxml.FXML;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.Spinner;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.stage.FileChooser;
import org.tribot.api.General;
import org.tribot.api2007.Equipment;
import org.tribot.api2007.Login;
import org.tribot.api2007.types.RSItem;
import org.tribot.api2007.types.RSItemDefinition;
import org.tribot.util.Util;
import scripts.fluffeesRedDragonKiller.data.AccountSettings;
import scripts.fluffeesRedDragonKiller.data.Variables;
import scripts.fluffeesapi.client.wrappers.ColoredText;
import scripts.fluffeesapi.client.wrappers.Printer;
import scripts.fluffeesapi.game.magic.Runes;
import scripts.fluffeesapi.game.magic.Spell;
import scripts.fluffeesapi.game.magic.Staff;
import scripts.fluffeesapi.scripting.javafx.glyphs.fontawesome.FontAwesomeIcon;
import scripts.fluffeesapi.scripting.javafx.jfoenix.controls.JFXButton;
import scripts.fluffeesapi.scripting.javafx.utilities.AbstractGUIController;
import scripts.fluffeesapi.scripting.javafx.utilities.GUI;
import scripts.fluffeesapi.scripting.minimalJson.Json;
import scripts.fluffeesapi.scripting.minimalJson.JsonObject;
import scripts.fluffeesapi.scripting.minimalJson.WriterConfig;
import scripts.fluffeesapi.scripting.types.interactables.*;

import java.io.*;
import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;

@DoNotRename
public class GUIController extends AbstractGUIController {

    private GUI gui;

    //Sidebar
    @FXML
    @DoNotRename
    private HBox sidebarMainSettings, sidebarEquipment, sidebarMagicSettings;

    //Buttons
    @FXML
    @DoNotRename
    private JFXButton btnLoad, btnSave, btnStart, btnGrabEquipment;

    //Close and Minimize
    @FXML
    @DoNotRename
    private FontAwesomeIcon minimizeButton, closeButton;

    @FXML
    @DoNotRename
    private TextField txtHelmet, txtCape, txtChest, txtShield, txtHands, txtRing, txtAmulet, txtAmmo, txtWeapon, txtLegs, txtFeet, txtStaffName;

    @FXML
    @DoNotRename
    private ComboBox cmbBankMethod, cmbAttackingMethod, cmbPotion, cmbAxe, cmbFood;

    @FXML
    @DoNotRename
    private ComboBox<Spell.COMBAT_SPELLS> cmbSpellName;

    @FXML
    @DoNotRename
    private ComboBox<Runes> cmbFirstRune, cmbSecondRune, cmbThirdRune, cmbAlternativeFirstRune, cmbAlternativeSecondRune, cmbAlternativeThirdRune;

    @FXML
    @DoNotRename
    private Spinner<Integer> spnFoodPer, spnPotionsPer, spnAmmoPer;

    @FXML
    @DoNotRename
    private Pane paneMainSettings, paneEquipment, paneMagicSettings;

    @FXML
    @DoNotRename
    private Label lblAltFirstRune, lblAltSecondRune, lblAltThirdRune;

    @FXML
    @DoNotRename
    public void minimizeButtonClicked() {
        getGUI().getStage().setIconified(true);
    }

    @FXML
    @DoNotRename
    public void closeButtonClicked() {
        Variables.get().setStopScript(true);
        Variables.get().setEndingReason("GUI Closed.");
        getGUI().close();
    }

    @FXML
    @DoNotRename
    public void btnStartClicked() {
        if (Variables.get().getAccountSettings() == null)
            Variables.get().setAccountSettings(createAccountSettings());
        getGUI().close();
    }

    @FXML
    @DoNotRename
    public void btnLoadClicked() {
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Load Settings");
        fileChooser.setInitialDirectory(new File(Util.getWorkingDirectory() + File.separator + "FluffeeScripts" + File.separator + "FluffeesRedDragonKiller" + File.separator));
        fileChooser.getExtensionFilters().addAll(
                new FileChooser.ExtensionFilter("Json", "*.json"),
                new FileChooser.ExtensionFilter("All Files", "*"));
        File file = fileChooser.showOpenDialog(getGUI().getStage());
        if (file == null) {
            General.println("Error, file is invalid");
            return;
        }
        BufferedReader bufferedReader = null;
        try {
            bufferedReader = new BufferedReader(new FileReader(file));
            JsonObject jsonObject = Json.parse(bufferedReader).asObject();
            Variables.get().setAccountSettings(AccountSettings.parseJson(jsonObject));
            loadAccountSettings();
            sidebarMainSettingsClicked();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @FXML
    @DoNotRename
    public void btnSaveClicked() {
        Variables.get().setAccountSettings(createAccountSettings());
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Save Settings");
        fileChooser.setInitialDirectory(new File(Util.getWorkingDirectory() + File.separator + "FluffeeScripts" + File.separator + "FluffeesRedDragonKiller" + File.separator));
        fileChooser.setInitialFileName(".json");
        fileChooser.getExtensionFilters().addAll(
                new FileChooser.ExtensionFilter("Json", "*.json"));
        File file = fileChooser.showSaveDialog(getGUI().getStage());
        if (file == null) {
            General.println("Error, file is invalid");
            return;
        }
        if (file.exists()) {
            file.delete();
        }
        BufferedWriter bufferedWriter = null;
        try {
            bufferedWriter = new BufferedWriter(new FileWriter(file, true));
            Variables.get().getAccountSettings().toJsonObject().writeTo(bufferedWriter, WriterConfig.PRETTY_PRINT);
            bufferedWriter.flush();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (bufferedWriter != null) try {
                bufferedWriter.close();
            } catch (IOException ioe2) {
                // just ignore it
            }
        }
    }

    @FXML
    @DoNotRename
    public void btnGrabEquipmentClicked() {
        if (Login.getLoginState() != Login.STATE.INGAME) {
            Printer.println(ColoredText.criticalMessage("Error: Cannot grab equipment when you're not logged in."));
        }
        RSItem[] equipment = Equipment.getItems();
        for (RSItem equip : equipment) {
            RSItemDefinition equipDefinition = equip.getDefinition();
            if (equipDefinition == null)
                continue;
            String equipName = equipDefinition.getName();
            if (equipName == null)
                continue;
            switch (equip.getEquipmentSlot()) {
                case HELMET:
                    txtHelmet.setText(equipName);
                    break;
                case ARROW:
                    txtAmmo.setText(equipName);
                    break;
                case BODY:
                    txtChest.setText(equipName);
                    break;
                case CAPE:
                    txtCape.setText(equipName);
                    break;
                case LEGS:
                    txtLegs.setText(equipName);
                    break;
                case RING:
                    txtRing.setText(equipName);
                    break;
                case BOOTS:
                    txtFeet.setText(equipName);
                    break;
                case AMULET:
                    txtAmulet.setText(equipName);
                    break;
                case GLOVES:
                    txtHands.setText(equipName);
                    break;
                case SHIELD:
                    txtShield.setText(equipName);
                    break;
                case WEAPON:
                    txtWeapon.setText(equipName);
                    if (cmbAttackingMethod.getSelectionModel().getSelectedIndex() == 1)
                        txtStaffName.setText(equipName);
                    break;
                default:
                    break;
            }

        }
    }

    @FXML
    @DoNotRename
    public void sidebarMainSettingsClicked() {
        paneMainSettings.toFront();
    }

    @FXML
    @DoNotRename
    public void sidebarEquipmentClicked() {
        paneEquipment.toFront();
    }

    @FXML
    @DoNotRename
    public void sidebarMagicSettingsClicked() {
        paneMagicSettings.toFront();
    }

    @FXML
    @DoNotRename
    public void cmbSpellNameChanged() {
        InteractableRune[] requiredRunes = cmbSpellName.getValue().getRunesRequired();
        updateAltRuneLabels(requiredRunes);
        cmbAlternativeFirstRune.setItems(FXCollections.observableArrayList(requiredRunes[0].getRune().getAlternativeRunes()));
        cmbAlternativeFirstRune.getSelectionModel().selectFirst();
        cmbAlternativeSecondRune.setItems(FXCollections.observableArrayList(requiredRunes[1].getRune().getAlternativeRunes()));
        cmbAlternativeSecondRune.getSelectionModel().selectFirst();
        cmbAlternativeThirdRune.setItems(FXCollections.observableArrayList(requiredRunes[2].getRune().getAlternativeRunes()));
        cmbAlternativeThirdRune.getSelectionModel().selectFirst();

    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        cmbAttackingMethod.setItems(FXCollections.observableArrayList("Ranged", "Mage"));
        cmbAttackingMethod.getSelectionModel().selectFirst();
        cmbBankMethod.setItems(FXCollections.observableArrayList("Fight Pits Bank"));
        cmbBankMethod.getSelectionModel().selectFirst();
        cmbAxe.setItems(FXCollections.observableArrayList("Bronze axe", "Iron axe", "Steel axe", "Black axe", "Mithril axe", "Adamant axe", "Rune axe", "Dragon axe", "Infernal adze"));
        cmbAxe.getSelectionModel().selectFirst();
        cmbFood.setItems(FXCollections.observableArrayList("Trout", "Salmon", "Bass", "Lobster", "Swordfish" , "Monkfish", "Shark", "Tuna"));
        cmbFood.getSelectionModel().selectFirst();
        cmbPotion.setItems(FXCollections.observableArrayList("None", "Ranging potion", "Magic potion"));
        cmbPotion.getSelectionModel().selectFirst();
        cmbSpellName.setItems(FXCollections.observableArrayList(Spell.COMBAT_SPELLS.values()));
        cmbSpellName.getSelectionModel().selectFirst();
        cmbFirstRune.setItems(FXCollections.observableArrayList(Runes.values()));
        cmbFirstRune.getSelectionModel().selectFirst();
        cmbSecondRune.setItems(FXCollections.observableArrayList(Runes.values()));
        cmbSecondRune.getSelectionModel().selectFirst();
        cmbThirdRune.setItems(FXCollections.observableArrayList(Runes.values()));
        cmbThirdRune.getSelectionModel().selectFirst();
        cmbSpellNameChanged();
    }

    private Spell determineSpell() {
        Runes[] chosenSpellRunes;
        int maxIndex = cmbSpellName.getValue().getRunesRequired().length;
        Runes[] chosenStaffRunes = new Runes[]{cmbFirstRune.getValue(), cmbSecondRune.getValue(), cmbThirdRune.getValue()};
        ArrayList<InteractableRune> spellRunes = new ArrayList<>();
        chosenSpellRunes = new Runes[]{cmbAlternativeFirstRune.getValue(), cmbAlternativeSecondRune.getValue(), cmbAlternativeThirdRune.getValue()};
        for (int i = 0; i < chosenSpellRunes.length; i++) {
            if (chosenSpellRunes[i] == Runes.NONE)
                continue;
            boolean found =  false;
            for (Runes staffRune : chosenStaffRunes) {
                if (staffRune == chosenSpellRunes[i]) {
                    found = true;
                    break;
                }
            }
            if (found)
                continue;
            spellRunes.add(new InteractableRune(chosenSpellRunes[i], cmbSpellName.getValue().getRunesRequired()[i].getQuantity()));
        }
        for (int i = 0; i < spellRunes.size(); i++) {
            for (int e = i+1; e < spellRunes.size(); e++) {
                if (spellRunes.get(i).getRune() == spellRunes.get(e).getRune()) {
                    if (spellRunes.get(i).getQuantity() >= spellRunes.get(e).getQuantity())
                        spellRunes.remove(e);
                    else
                        spellRunes.remove(i);
                }
            }
        }
        return new Spell(cmbSpellName.getValue().getLevelRequired(), cmbSpellName.getValue().getAutocastComponentID(), cmbSpellName.getValue().getAutocastSetting(),
                cmbSpellName.getValue().isMembers(), spellRunes.toArray(new InteractableRune[spellRunes.size()]), cmbSpellName.getValue().getSpellName(), cmbSpellName.getValue().name());
    }

    private AccountSettings createAccountSettings() {
        int numberEquipment = countEquipment();
        boolean useRanged = false;
        InteractableEquipment[] equipment = new InteractableEquipment[numberEquipment];
        int index = 0;
        if (!txtHelmet.getText().trim().isEmpty()) {
            equipment[index] = new InteractableEquipment(txtHelmet.getText().trim(), Interactable.DEFAULT_ID, "Wear", Equipment.SLOTS.HELMET);
            index++;
        }
        if (!txtAmulet.getText().trim().isEmpty()) {
            equipment[index] = new InteractableEquipment(txtAmulet.getText().trim(), Interactable.DEFAULT_ID, "Wear", Equipment.SLOTS.AMULET);
            index++;
        }
        if (!txtCape.getText().trim().isEmpty()) {
            equipment[index] = new InteractableEquipment(txtCape.getText().trim(), Interactable.DEFAULT_ID, "Wear", Equipment.SLOTS.CAPE);
            index++;
        }
        if (!txtChest.getText().trim().isEmpty()) {
            equipment[index] = new InteractableEquipment(txtChest.getText().trim(), Interactable.DEFAULT_ID, "Wear", Equipment.SLOTS.BODY);
            index++;
        }
        if (!txtWeapon.getText().trim().isEmpty()) {
            equipment[index] = new InteractableEquipment(txtWeapon.getText().trim(), Interactable.DEFAULT_ID, "Wield", Equipment.SLOTS.WEAPON);
            index++;
        }
        if (!txtShield.getText().trim().isEmpty()) {
            equipment[index] = new InteractableEquipment(txtShield.getText().trim(), Interactable.DEFAULT_ID, "Wear", Equipment.SLOTS.SHIELD);
            index++;
        }
        if (!txtLegs.getText().trim().isEmpty()) {
            equipment[index] = new InteractableEquipment(txtLegs.getText().trim(), Interactable.DEFAULT_ID, "Wear", Equipment.SLOTS.LEGS);
            index++;
        }
        if (!txtHands.getText().trim().isEmpty()) {
            equipment[index] = new InteractableEquipment(txtHands.getText().trim(), Interactable.DEFAULT_ID, "Wear", Equipment.SLOTS.GLOVES);
            index++;
        }
        if (!txtFeet.getText().trim().isEmpty()) {
            equipment[index] = new InteractableEquipment(txtFeet.getText().trim(), Interactable.DEFAULT_ID, "Wear", Equipment.SLOTS.BOOTS);
            index++;
        }
        if (!txtRing.getText().trim().isEmpty()) {
            equipment[index] = new InteractableEquipment(txtRing.getText().trim(), Interactable.DEFAULT_ID, "Wear", Equipment.SLOTS.RING);
            index++;
        }
        if (cmbAttackingMethod.getValue().toString().startsWith("R")) {
            useRanged = true;
        }
        InteractableItem axe = new InteractableItem(cmbAxe.getValue().toString(), 1, Interactable.DEFAULT_ID, "Wield");
        InteractableItem food = new InteractableItem(cmbFood.getValue().toString(), spnFoodPer.getValue().intValue(), Interactable.DEFAULT_ID, "Eat");
        if (useRanged) {
            InteractableEquipment ammo = new InteractableEquipment(txtAmmo.getText().trim(), spnAmmoPer.getValue().intValue(), Interactable.DEFAULT_ID, "Wield", Equipment.SLOTS.ARROW);
            if (cmbPotion.getValue().toString().startsWith("N"))
                return new AccountSettings(equipment, axe, food, ammo);
            else
                return new AccountSettings(equipment, new InteractablePotion(cmbPotion.getValue().toString(), spnPotionsPer.getValue().intValue(), InteractablePotion.POTION_TYPE.THREE_DOSE),
                        axe, food, ammo);
        } else {
            Staff staff = new Staff(txtStaffName.getText(), cmbFirstRune.getValue(), cmbSecondRune.getValue(), cmbThirdRune.getValue());
            if (cmbPotion.getValue().toString().startsWith("N"))
                return new AccountSettings(equipment, axe, food, staff, determineSpell(), spnAmmoPer.getValue().intValue());
            else
                return new AccountSettings(equipment, new InteractablePotion(cmbPotion.getValue().toString(), spnPotionsPer.getValue().intValue(), InteractablePotion.POTION_TYPE.THREE_DOSE),
                        axe, food, staff, determineSpell(), spnAmmoPer.getValue().intValue());
        }
    }

    private int countEquipment() {
        int count = 0;
        if (!txtHelmet.getText().trim().isEmpty())
            count++;
        if (!txtAmulet.getText().trim().isEmpty())
            count++;
        if (!txtCape.getText().trim().isEmpty())
            count++;
        if (!txtChest.getText().trim().isEmpty())
            count++;
        if (!txtWeapon.getText().trim().isEmpty())
            count++;
        if (!txtShield.getText().trim().isEmpty())
            count++;
        if (!txtLegs.getText().trim().isEmpty())
            count++;
        if (!txtHands.getText().trim().isEmpty())
            count++;
        if (!txtFeet.getText().trim().isEmpty())
            count++;
        if (!txtRing.getText().trim().isEmpty())
            count++;
        return count;
    }

    private void loadAccountSettings() {
        if (Variables.get().getAccountSettings() == null)
            return;
        for (InteractableEquipment equipment : Variables.get().getAccountSettings().getEquipment()) {
            switch (equipment.getSlot()) {
                case HELMET:
                    txtHelmet.setText(equipment.getName());
                    break;
                case BODY:
                    txtChest.setText(equipment.getName());
                    break;
                case CAPE:
                    txtCape.setText(equipment.getName());
                    break;
                case LEGS:
                    txtLegs.setText(equipment.getName());
                    break;
                case RING:
                    txtRing.setText(equipment.getName());
                    break;
                case BOOTS:
                    txtFeet.setText(equipment.getName());
                    break;
                case AMULET:
                    txtAmulet.setText(equipment.getName());
                    break;
                case GLOVES:
                    txtHands.setText(equipment.getName());
                    break;
                case SHIELD:
                    txtShield.setText(equipment.getName());
                    break;
                case WEAPON:
                    txtWeapon.setText(equipment.getName());
                    break;
                default:
                    break;
            }
        }
        if (Variables.get().getAccountSettings().isUseRanged()) {
            txtAmmo.setText(Variables.get().getAccountSettings().getAmmo().getName());
            spnAmmoPer.getValueFactory().setValue(Variables.get().getAccountSettings().getAmmo().getQuantity());
            cmbAttackingMethod.getSelectionModel().selectFirst();
        } else {
            spnAmmoPer.getValueFactory().setValue(Variables.get().getAccountSettings().getCastsPerRun());
            cmbAttackingMethod.getSelectionModel().select(1);
            txtStaffName.setText(Variables.get().getAccountSettings().getStaff().getStaffName());
            cmbFirstRune.getSelectionModel().select(Variables.get().getAccountSettings().getStaff().getFirstRune());
            cmbSecondRune.getSelectionModel().select(Variables.get().getAccountSettings().getStaff().getSecondRune());
            cmbThirdRune.getSelectionModel().select(Variables.get().getAccountSettings().getStaff().getThirdRune());
            cmbSpellName.getSelectionModel().select(Spell.COMBAT_SPELLS.valueOf(Variables.get().getAccountSettings().getSpell().getEnumSpellName()));
            if (Variables.get().getAccountSettings().getSpell().getRunesRequired().length <= 1) {
                cmbAlternativeFirstRune.getSelectionModel().select(Variables.get().getAccountSettings().getSpell().getRunesRequired()[0].getRune());
                cmbAlternativeSecondRune.getSelectionModel().select(Runes.NONE);
                cmbAlternativeThirdRune.getSelectionModel().select(Runes.NONE);
            } else if (Variables.get().getAccountSettings().getSpell().getRunesRequired().length <= 2) {
                cmbAlternativeFirstRune.getSelectionModel().select(Variables.get().getAccountSettings().getSpell().getRunesRequired()[0].getRune());
                cmbAlternativeSecondRune.getSelectionModel().select(Variables.get().getAccountSettings().getSpell().getRunesRequired()[1].getRune());
                cmbAlternativeThirdRune.getSelectionModel().select(Runes.NONE);
            } else {
                cmbAlternativeFirstRune.getSelectionModel().select(Variables.get().getAccountSettings().getSpell().getRunesRequired()[0].getRune());
                cmbAlternativeSecondRune.getSelectionModel().select(Variables.get().getAccountSettings().getSpell().getRunesRequired()[1].getRune());
                cmbAlternativeThirdRune.getSelectionModel().select(Variables.get().getAccountSettings().getSpell().getRunesRequired()[2].getRune());
            }
            updateAltRuneLabels(Variables.get().getAccountSettings().getSpell().getRunesRequired());
        }
        cmbAxe.getSelectionModel().select(Variables.get().getAccountSettings().getAxe().getName());
        spnFoodPer.getValueFactory().setValue(Variables.get().getAccountSettings().getFood().getQuantity());
        cmbFood.getSelectionModel().select(Variables.get().getAccountSettings().getFood().getName());
        if (Variables.get().getAccountSettings().isUsePotions()) {
            cmbPotion.getSelectionModel().select(Variables.get().getAccountSettings().getPotion().getPotionName());
            spnPotionsPer.getValueFactory().setValue(Variables.get().getAccountSettings().getPotion().getQuantity());
        }
    }

    private void updateAltRuneLabels(InteractableRune[] runes) {
        if (runes.length <= 1) {
            lblAltFirstRune.setText("Alternative First Rune (" + runes[0].getQuantity() + " per cast)");
            lblAltSecondRune.setText("Alternative Third Rune (0 per cast)");
            lblAltThirdRune.setText("Alternative Third Rune (0 per cast)");
        } else if (runes.length <= 2) {
            lblAltFirstRune.setText("Alternative First Rune (" + runes[0].getQuantity() + " per cast)");
            lblAltSecondRune.setText("Alternative Second Rune (" + runes[1].getQuantity() + " per cast)");
            lblAltThirdRune.setText("Alternative Third Rune (0 per cast)");
        } else {
            lblAltFirstRune.setText("Alternative First Rune (" + runes[0].getQuantity() + " per cast)");
            lblAltSecondRune.setText("Alternative Second Rune (" + runes[1].getQuantity() + " per cast)");
            lblAltThirdRune.setText("Alternative Third Rune (" + runes[2].getQuantity() + " per cast)");
        }

    }
}
