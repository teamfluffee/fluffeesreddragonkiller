package scripts.fluffeesRedDragonKiller.gui;

public class CSSContents {

    public static String cssContents =
            ".sidebar{\n" +
            "    -fx-background-color: #2e3532;\n" +
            "}\n" +
            ".sidebar-btns:hover{\n" +
            "    -fx-background-color: #242927;\n" +
            "}\n" +
            ".inner-btns{\n" +
            "    -fx-background-color: #bbb5b5;\n" +
            "}\n" +
            ".inner-btns:hover{\n" +
            "    -fx-background-color: #aea7a7;\n" +
            "}\n" +
            ".close-btns{\n" +
            "    -fx-fill: #797d8a;\n" +
            "}\n" +
            ".close-btns:hover{\n" +
            "    -fx-fill: #53565f;\n" +
            "}\n" +
            ".script-btns{\n" +
            "    -fx-background-color: #963131;\n" +
            "}\n" +
            ".script-btns:hover{\n" +
            "    -fx-background-color: #c34646;\n" +
            "}\n";


}
