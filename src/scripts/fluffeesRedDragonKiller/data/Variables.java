package scripts.fluffeesRedDragonKiller.data;

import org.tribot.api2007.types.RSTile;
import scripts.fluffeesapi.client.wrappers.ColoredText;
import scripts.fluffeesapi.client.wrappers.Printer;
import scripts.fluffeesapi.data.interactables.Interactable;
import scripts.fluffeesapi.data.interactables.InteractableItem;
import scripts.fluffeesapi.game.camera.ACamera;
import scripts.fluffeesapi.scripting.exceptions.PriceNotFoundException;
import scripts.fluffeesapi.web.PriceLookup;

import java.util.HashMap;

public class Variables {

    // Instance manipulation
    private Variables() {}
    private static final Variables VARIABLES = new Variables();
    public static Variables get() {
        return VARIABLES;
    }

    private ACamera aCamera = new ACamera();
    private RSTile currentDragonTile = null;
    private AccountSettings accountSettings;
    private String scriptStatus = "Loading", endingReason = "Unknown reason.";
    private int profit = 0, bankfailCounter = 0;
    private long lastTeleportTime = 0;
    private HashMap<Integer, Integer> itemPrices = new HashMap<>();

    private boolean stopScript = false;
    private boolean lootGained = true;

    private Interactable fightPitsExit = new Interactable("Cave exit", Interactable.DEFAULT_ID, "Enter");
    private Interactable karamjaCaveExit = new Interactable("Climbing rope", Interactable.DEFAULT_ID, "Climb");
    private Interactable dungeonEntrance = new Interactable("Dungeon entrance", Interactable.DEFAULT_ID, "Enter");
    private Interactable saniboch = new Interactable("Saniboch", Interactable.DEFAULT_ID, "Pay");
    private Interactable firstVines = new Interactable("Vines", Interactable.DEFAULT_ID, "Chop-down");
    private Interactable enterSteppingStone = new Interactable("Stepping stone", Interactable.DEFAULT_ID, "Jump-from");
    private Interactable thirdVines = new Interactable("Vines", Interactable.DEFAULT_ID, "Chop-down");
    private Interactable fourthLogBalanceExit = new Interactable("Log balance", Interactable.DEFAULT_ID, "Walk-across");
    private Interactable redDragons = new Interactable("Red dragon", 6, "Attack", Interactable.DEFAULT_ID);
    private InteractableItem coins = new InteractableItem("Coins", "Use", Interactable.DEFAULT_ID, 875);

    public ACamera getaCamera() {
        return aCamera;
    }

    public void setaCamera(ACamera aCamera) {
        this.aCamera = aCamera;
    }

    public boolean isStopScript() {
        return stopScript;
    }

    public void setStopScript(boolean stopScript) {
        this.stopScript = stopScript;
    }

    public Interactable getFightPitsExit() {
        return fightPitsExit;
    }

    public Interactable getKaramjaCaveExit() {
        return karamjaCaveExit;
    }

    public Interactable getDungeonEntrance() {
        return dungeonEntrance;
    }

    public Interactable getSaniboch() {
        return saniboch;
    }

    public Interactable getFirstVines() {
        return firstVines;
    }

    public Interactable getEnterSteppingStone() {
        return enterSteppingStone;
    }

    public Interactable getThirdVines() {
        return thirdVines;
    }

    public Interactable getFourthLogBalanceExit() {
        return fourthLogBalanceExit;
    }

    public Interactable getRedDragons() {
        return redDragons;
    }

    public InteractableItem getCoins() {
        return coins;
    }

    public RSTile getCurrentDragonTile() {
        return currentDragonTile;
    }

    public void setCurrentDragonTile(RSTile currentDragonTile) {
        this.currentDragonTile = currentDragonTile;
    }

    public AccountSettings getAccountSettings() {
        return accountSettings;
    }

    public void setAccountSettings(AccountSettings accountSettings) {
        this.accountSettings = accountSettings;
    }

    public void setLastTeleportTime(long lastTeleportTime) {
        this.lastTeleportTime = lastTeleportTime;
    }

    public String getScriptStatus() {
        return scriptStatus;
    }

    public void setScriptStatus(String scriptStatus) {
        this.scriptStatus = scriptStatus;
    }

    public int getProfit() {
        return profit;
    }

    public void addToProfit(int itemID, int itemCount) {
        if (itemPrices.containsKey(Integer.valueOf(itemID))) {
            profit += (itemCount * itemPrices.get(Integer.valueOf(itemID)));
        } else {
            Integer price = 0;
            try {
                price = Integer.valueOf(PriceLookup.getPrice(itemID));
            } catch (PriceNotFoundException exception) {
                Printer.println(ColoredText.criticalMessage("Error: Price for item id " + itemID + " could not be found. Not recording it to profit this time."));
            }
            profit += (itemCount * price);
        }
    }

    public void removeFromProfit(int itemID, int itemCount) {
        if (itemPrices.containsKey(Integer.valueOf(itemID))) {
            profit -= (itemCount * itemPrices.get(Integer.valueOf(itemID)));
        } else {
            Integer price = 0;
            try {
                price = Integer.valueOf(PriceLookup.getPrice(itemID));
            } catch (PriceNotFoundException exception) {
                Printer.println(ColoredText.criticalMessage("Error: Price for item id " + itemID + " could not be found. Not recording it to profit this time."));
            }
            profit -= (itemCount * price);
        }
    }

    public boolean isLootGained() {
        return lootGained;
    }

    public void setLootGained(boolean lootGained) {
        this.lootGained = lootGained;
    }

    public boolean canTeleport() {
        if (lastTeleportTime == 0 || System.currentTimeMillis() - lastTeleportTime > (60*20*1000))
            return true;
        return false;
    }

    public int getTimeToTeleport() {
        return (int) ((60*20*1000) - (System.currentTimeMillis() - lastTeleportTime));
    }

    public void incrementBankfailCounter() {
        bankfailCounter++;
    }

    public void resetBankfailCounter() {
        bankfailCounter = 0;
    }

    public int getBankfailCounter() {
        return bankfailCounter;
    }

    public String getEndingReason() {
        return endingReason;
    }

    public void setEndingReason(String endingReason) {
        this.endingReason = endingReason;
    }
}
