package scripts.fluffeesRedDragonKiller.data;

import org.tribot.api2007.types.RSArea;
import org.tribot.api2007.types.RSTile;

public class Constants {

    public static final int PAID_TO_ENTER_VARBIT = 5628;
    public static final int FIGHT_PITS_VARBIT = 4605;
    public static final int ACHIEVEMENT_DIARIES_VARBIT = 3612;
    public static final int MINIGAME_VARBIT = 3217;

    public static final int HOLD_SPACEBAR = 0;
    public static final int TAP_SPACEBAR = 1;
    public static final int CLICK_COTINUE_WITH_MOUSE = 2;

    public static final RSArea KARAMJA_CAVE = new RSArea(new RSTile(2846, 9581, 0), new RSTile(2868, 9565, 0));

    //Brimhaven Dungeon Areas
    public static final RSArea BRIMHAVEN_DUNGEON = new RSArea(new RSTile(2625, 9600, 0), new RSTile(2750, 9410, 0));
    public static final RSArea BRIMHAVEN_DUNGEON_FIRST_ROOM = new RSArea(new RSTile(2690, 9577, 0), new RSTile(2715,9552, 0));
    public static final RSArea BRIMHAVEN_DUNGEON_SECOND_ROOM = new RSArea(new RSTile(2645, 9575, 0), new RSTile(2691,9560, 0));
    public static final RSArea BRIMHAVEN_DUNGEON_THIRD_ROOM = new RSArea(new RSTile(2630, 9558, 0), new RSTile(2673,9475, 0));
    public static final RSArea BRIMHAVEN_DUNGEON_THIRD_ROOM_PART_1 = new RSArea(new RSTile [] { new RSTile(2635, 9559, 0),new RSTile(2652, 9559, 0),new RSTile(2652, 9554, 0),new RSTile(2653, 9552, 0),new RSTile(2654, 9552, 0),new RSTile(2654, 9550, 0),new RSTile(2652, 9549, 0),new RSTile(2652, 9547, 0),new RSTile(2651, 9546, 0),new RSTile(2650, 9545, 0),new RSTile(2651, 9543, 0),new RSTile(2652, 9541, 0),new RSTile(2654, 9541, 0),new RSTile(2654, 9539, 0),new RSTile(2655, 9539, 0),new RSTile(2656, 9539, 0),new RSTile(2657, 9539, 0),new RSTile(2657, 9537, 0),new RSTile(2658, 9536, 0),new RSTile(2658, 9532, 0),new RSTile(2645, 9532, 0),new RSTile(2628, 9532, 0),new RSTile(2627, 9535, 0),new RSTile(2628, 9539, 0),new RSTile(2630, 9540, 0),new RSTile(2632, 9540, 0),new RSTile(2633, 9542, 0),new RSTile(2632, 9544, 0),new RSTile(2631, 9546, 0),new RSTile(2629, 9547, 0),new RSTile(2629, 9549, 0),new RSTile(2635, 9550, 0),new RSTile(2637, 9552, 0),new RSTile(2638, 9550, 0),new RSTile(2640, 9549, 0),new RSTile(2642, 9548, 0),new RSTile(2643, 9549, 0),new RSTile(2643, 9550, 0),new RSTile(2640, 9552, 0),new RSTile(2638, 9552, 0),new RSTile(2637, 9553, 0),new RSTile(2636, 9553, 0),new RSTile(2635, 9555, 0) });
    //Lowest Y = 9532
    public static final RSArea BRIMHAVEN_DUNGEON_THIRD_ROOM_PART_2 = new RSArea(new RSTile [] { new RSTile(2628, 9531, 0),new RSTile(2659, 9531, 0),new RSTile(2658, 9521, 0),new RSTile(2649, 9514, 0),new RSTile(2636, 9514, 0),new RSTile(2632, 9514, 0),new RSTile(2628, 9514, 0) });
    //Lowest Y = 9514
    public static final RSArea BRIMHAVEN_DUNGEON_THIRD_ROOM_PART_3 = new RSArea(new RSTile [] { new RSTile(2641, 9513, 0),new RSTile(2651, 9513, 0),new RSTile(2655, 9506, 0),new RSTile(2659, 9505, 0),new RSTile(2660, 9508, 0),new RSTile(2662, 9507, 0),new RSTile(2662, 9487, 0),new RSTile(2641, 9487, 0) });
    //Rightmost X = 9562 (Really 9561)
    public static final RSArea BRIMHAVEN_DUNGEON_THIRD_ROOM_PART_4 = new RSArea(new RSTile [] { new RSTile(2662, 9511, 0),new RSTile(2670, 9513, 0),new RSTile(2672, 9509, 0),new RSTile(2671, 9502, 0),new RSTile(2674, 9502, 0),new RSTile(2674, 9497, 0),new RSTile(2675, 9483, 0),new RSTile(2675, 9476, 0),new RSTile(2664, 9476, 0),new RSTile(2662, 9476, 0) });

    public static final RSArea BRIMHAVEN_DUNGEON_FOURTH_ROOM = new RSArea(new RSTile(2673, 9506, 0), new RSTile(2683,9497, 0));
    public static final RSArea BRIMHAVEN_RED_DRAGONS_AREA = new RSArea(new RSTile [] { new RSTile(2686, 9507, 0),new RSTile(2686, 9509, 0),new RSTile(2688, 9510, 0),new RSTile(2691, 9510, 0),new RSTile(2691, 9511, 0),new RSTile(2691, 9518, 0),new RSTile(2691, 9520, 0),new RSTile(2696, 9520, 0),new RSTile(2696, 9529, 0),new RSTile(2701, 9530, 0),new RSTile(2701, 9542, 0),new RSTile(2695, 9553, 0),new RSTile(2707, 9554, 0),new RSTile(2714, 9555, 0),new RSTile(2718, 9552, 0),new RSTile(2720, 9541, 0),new RSTile(2726, 9541, 0),new RSTile(2728, 9528, 0),new RSTile(2730, 9521, 0),new RSTile(2733, 9519, 0),new RSTile(2735, 9514, 0),new RSTile(2734, 9510, 0),new RSTile(2717, 9508, 0),new RSTile(2722, 9507, 0),new RSTile(2727, 9507, 0),new RSTile(2729, 9507, 0),new RSTile(2726, 9499, 0),new RSTile(2717, 9500, 0),new RSTile(2714, 9498, 0),new RSTile(2712, 9497, 0),new RSTile(2711, 9496, 0),new RSTile(2707, 9496, 0),new RSTile(2699, 9499, 0),new RSTile(2694, 9499, 0) });
    public static final RSArea ATTACKABLE_AREA = new RSArea(new RSTile(2665, 9519, 0), new RSTile(2709,9505, 0));

    //Brimhaven Dungeon Tiles
    public static final RSTile FIRST_ROOM_VINES_TILE = new RSTile(2690, 9564, 0);
    public static final RSTile SECOND_ROOM_STEPPING_STONES_TILE = new RSTile(2649, 9563, 0);
    public static final RSTile THIRD_ROOM_FIRST_TILE = new RSTile(2640, 9529, 0);
    public static final RSTile THIRD_ROOM_SECOND_TILE = new RSTile(2647, 9510, 0);
    public static final RSTile THIRD_ROOM_THIRD_TILE = new RSTile(2664, 9503, 0);
    public static final RSTile THIRD_ROOM_VINES_TILE = new RSTile(2673, 9499, 0);
    public static final RSTile FOURTH_ROOM_LOG_BALANCE_TILE = new RSTile(2682, 9506, 0);
    public static final RSTile RED_DRAGON_SAFESPOT = new RSTile(2702, 9512, 0);

    public static final RSTile DUNGEON_ENTRANCE = new RSTile(2745, 3152, 0);

    public static final RSTile FIGHT_PITS_BANK = new RSTile(2445, 5180, 0);
    public static final RSTile FIGHT_PITS_ENTRANCE = new RSTile(2480, 5174, 0);
    public static final RSTile KARAMJA_CAVE_ROPE = new RSTile(2856, 9570, 0);



}
