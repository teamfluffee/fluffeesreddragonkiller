package scripts.fluffeesRedDragonKiller.data;

import org.tribot.api2007.Skills;
import scripts.fluffeesapi.data.interactables.InteractableEquipment;
import scripts.fluffeesapi.data.interactables.InteractableItem;
import scripts.fluffeesapi.data.interactables.InteractablePotion;
import scripts.fluffeesapi.game.magic.Spell;
import scripts.fluffeesapi.game.magic.Staff;
import scripts.fluffeesapi.scripting.minimalJson.JsonArray;
import scripts.fluffeesapi.scripting.minimalJson.JsonObject;

public class AccountSettings {

    private InteractableEquipment[] equipment;
    private InteractablePotion potion;
    private InteractableItem axe, food;
    private InteractableEquipment ammo;
    private Staff staff;
    private Spell spell;
    private boolean useRanged, usePotions;
    private Skills.SKILLS trainingSkill = Skills.SKILLS.HITPOINTS;
    private int castsPerRun, chatABC2Sleep = 300, chatABC2Counter = 0, attackingABC2Sleep = 300, attackingABC2Counter = 0,
            vinesABC2Sleep = 300, vinesABC2Counter = 0, steppingStonesABC2Sleep = 300, steppingStonesABC2Counter = 0, teleportingABC2Sleep = 300, teleportingABC2Counter = 0,
            doorsABC2Sleep = 300, doorsABC2Counter = 0, clickContinue = 0, numberOfEquipment;

    public AccountSettings(InteractableEquipment[] equipment, InteractablePotion potion, InteractableItem axe, InteractableItem food, InteractableEquipment ammo) {
        this.equipment = equipment;
        this.potion = potion;
        this.axe = axe;
        this.food = food;
        this.ammo = ammo;
        this.useRanged = true;
        this.usePotions = true;
        this.numberOfEquipment = equipment.length;
        this.trainingSkill = this.useRanged ? Skills.SKILLS.RANGED : Skills.SKILLS.MAGIC;
    }

    public AccountSettings(InteractableEquipment[] equipment, InteractablePotion potion, InteractableItem axe, InteractableItem food, Staff staff, Spell spell, int castsPerRun) {
        this.equipment = equipment;
        this.potion = potion;
        this.axe = axe;
        this.food = food;
        this.staff = staff;
        this.spell = spell;
        this.castsPerRun = castsPerRun;
        this.useRanged = false;
        this.usePotions = true;
        this.numberOfEquipment = equipment.length;
        this.trainingSkill = this.useRanged ? Skills.SKILLS.RANGED : Skills.SKILLS.MAGIC;
    }

    public AccountSettings(InteractableEquipment[] equipment, InteractableItem axe, InteractableItem food, InteractableEquipment ammo) {
        this.equipment = equipment;
        this.axe = axe;
        this.food = food;
        this.ammo = ammo;
        this.useRanged = true;
        this.usePotions = false;
        this.numberOfEquipment = equipment.length;
        this.trainingSkill = this.useRanged ? Skills.SKILLS.RANGED : Skills.SKILLS.MAGIC;
    }

    public AccountSettings(InteractableEquipment[] equipment, InteractableItem axe, InteractableItem food, Staff staff, Spell spell, int castsPerRun) {
        this.equipment = equipment;
        this.axe = axe;
        this.food = food;
        this.staff = staff;
        this.spell = spell;
        this.castsPerRun = castsPerRun;
        this.useRanged = false;
        this.usePotions = false;
        this.numberOfEquipment = equipment.length;
        this.trainingSkill = this.useRanged ? Skills.SKILLS.RANGED : Skills.SKILLS.MAGIC;
    }

    public int getChatABC2Sleep() {
        return chatABC2Sleep;
    }

    public void setChatABC2Sleep(int chatABC2Sleep) {
        this.chatABC2Sleep = chatABC2Sleep;
    }

    public int getChatABC2Counter() {
        return chatABC2Counter;
    }

    public void setChatABC2Counter(int chatABC2Counter) {
        this.chatABC2Counter = chatABC2Counter;
    }

    public int getAttackingABC2Sleep() {
        return attackingABC2Sleep;
    }

    public void setAttackingABC2Sleep(int attackingABC2Sleep) {
        this.attackingABC2Sleep = attackingABC2Sleep;
    }

    public int getAttackingABC2Counter() {
        return attackingABC2Counter;
    }

    public void incrementAttackingABC2Counter() {
        this.attackingABC2Counter++;
    }

    public int getVinesABC2Sleep() {
        return vinesABC2Sleep;
    }

    public void setVinesABC2Sleep(int vinesABC2Sleep) {
        this.vinesABC2Sleep = vinesABC2Sleep;
    }

    public int getVinesABC2Counter() {
        return vinesABC2Counter;
    }

    public void incrementVinesABC2Counter() {
        this.vinesABC2Counter++;
    }

    public int getSteppingStonesABC2Sleep() {
        return steppingStonesABC2Sleep;
    }

    public void setSteppingStonesABC2Sleep(int steppingStonesABC2Sleep) {
        this.steppingStonesABC2Sleep = steppingStonesABC2Sleep;
    }

    public int getSteppingStonesABC2Counter() {
        return steppingStonesABC2Counter;
    }

    public void incrementSteppingStonesABC2Counter() {
        this.steppingStonesABC2Counter++;
    }

    public int getTeleportingABC2Sleep() {
        return teleportingABC2Sleep;
    }

    public void setTeleportingABC2Sleep(int teleportingABC2Sleep) {
        this.teleportingABC2Sleep = teleportingABC2Sleep;
    }

    public int getTeleportingABC2Counter() {
        return teleportingABC2Counter;
    }

    public void incrementTeleportingABC2Counter() {
        this.teleportingABC2Counter++;
    }

    public int getDoorsABC2Sleep() {
        return doorsABC2Sleep;
    }

    public void setDoorsABC2Sleep(int doorsABC2Sleep) {
        this.doorsABC2Sleep = doorsABC2Sleep;
    }

    public int getDoorsABC2Counter() {
        return doorsABC2Counter;
    }

    public void incrementDoorsABC2Counter() {
        this.doorsABC2Counter++;
    }

    public InteractablePotion getPotion() {
        return potion;
    }

    public InteractableItem getAxe() {
        return axe;
    }

    public InteractableItem getFood() {
        return food;
    }

    public int getClickContinue() {
        return clickContinue;
    }

    public int getNumberOfEquipment() {
        return numberOfEquipment;
    }

    public InteractableEquipment[] getEquipment() {
        return equipment;
    }

    public InteractableEquipment getAmmo() {
        return ammo;
    }

    public Spell getSpell() {
        return spell;
    }

    public Staff getStaff() {
        return staff;
    }

    public int getCastsPerRun() {
        return castsPerRun;
    }

    public boolean isUseRanged() {
        return useRanged;
    }

    public boolean isUsePotions() {
        return usePotions;
    }

    public Skills.SKILLS getTrainingSkill() {
        return trainingSkill;
    }

    public JsonObject toJsonObject() {
        JsonObject jsonObject = new JsonObject();
        JsonArray jsonEquipment = new JsonArray();
        for (InteractableEquipment equipmentPiece : this.equipment)
            jsonEquipment.add(equipmentPiece.toJsonObject());
        jsonObject.add("equipment", jsonEquipment);
        if (usePotions)
            jsonObject.add("potion", this.potion.toJsonObject());
        jsonObject.add("axe", this.axe.toJsonObject());
        jsonObject.add("food", this.food.toJsonObject());
        if (this.useRanged)
            jsonObject.add("ammo", this.ammo.toJsonObject());
        else {
            jsonObject.add("staff", this.staff.toJsonObject());
            jsonObject.add("spell", this.spell.toJsonObject());
            jsonObject.add("castsPerRun", this.castsPerRun);
        }
        return jsonObject;
    }

    public static AccountSettings parseJson(JsonObject jsonObject) {
        JsonArray jsonEquipment = jsonObject.get("equipment").asArray();
        InteractableEquipment[] equipment = new InteractableEquipment[jsonEquipment.size()];
        for (int i = 0; i < equipment.length; i++)
            equipment[i] = InteractableEquipment.parseJson(jsonEquipment.get(i).asObject());
        if (jsonObject.contains("ammo")) {
            if (jsonObject.contains("potion"))
                return new AccountSettings(equipment, InteractablePotion.parseJson(jsonObject.get("potion").asObject()), InteractableItem.parseJson(jsonObject.get("axe").asObject()),
                        InteractableItem.parseJson(jsonObject.get("food").asObject()), InteractableEquipment.parseJson(jsonObject.get("ammo").asObject()));
            else
                return new AccountSettings(equipment, InteractableItem.parseJson(jsonObject.get("axe").asObject()), InteractableItem.parseJson(jsonObject.get("food").asObject()),
                        InteractableEquipment.parseJson(jsonObject.get("ammo").asObject()));
        } else {
            if (jsonObject.contains("potion"))
                return new AccountSettings(equipment, InteractablePotion.parseJson(jsonObject.get("potion").asObject()), InteractableItem.parseJson(jsonObject.get("axe").asObject()),
                        InteractableItem.parseJson(jsonObject.get("food").asObject()), Staff.parseJson(jsonObject.get("staff").asObject()),
                        Spell.parseJson(jsonObject.get("spell").asObject()), jsonObject.get("castsPerRun").asInt());
            else
                return new AccountSettings(equipment, InteractableItem.parseJson(jsonObject.get("axe").asObject()), InteractableItem.parseJson(jsonObject.get("food").asObject()),
                        Staff.parseJson(jsonObject.get("staff").asObject()), Spell.parseJson(jsonObject.get("spell").asObject()), jsonObject.get("castsPerRun").asInt());

        }

    }
}
