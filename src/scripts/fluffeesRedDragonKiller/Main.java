package scripts.fluffeesRedDragonKiller;

import org.tribot.api.General;
import org.tribot.api.Timing;
import org.tribot.api2007.Login;
import org.tribot.api2007.Skills;
import org.tribot.api2007.WebWalking;
import org.tribot.script.ScriptManifest;
import org.tribot.script.interfaces.*;
import org.tribot.util.Util;
import scripts.fluffeesRedDragonKiller.data.AccountSettings;
import scripts.fluffeesRedDragonKiller.data.Variables;
import scripts.fluffeesRedDragonKiller.gui.CSSContents;
import scripts.fluffeesRedDragonKiller.gui.FXML1;
import scripts.fluffeesRedDragonKiller.gui.GUIController;
import scripts.fluffeesRedDragonKiller.nodes.decisionNodes.ShouldBank;
import scripts.fluffeesRedDragonKiller.nodes.decisionNodes.noBank.AtRedDragons;
import scripts.fluffeesRedDragonKiller.nodes.decisionNodes.noBank.ShouldEquipItems;
import scripts.fluffeesRedDragonKiller.nodes.decisionNodes.noBank.ShouldGrabIDs;
import scripts.fluffeesRedDragonKiller.nodes.decisionNodes.noBank.combat.*;
import scripts.fluffeesRedDragonKiller.nodes.decisionNodes.noBank.walkToDragons.InBrimhavenDungeon;
import scripts.fluffeesRedDragonKiller.nodes.decisionNodes.noBank.walkToDragons.*;
import scripts.fluffeesRedDragonKiller.nodes.decisionNodes.yesBank.*;
import scripts.fluffeesRedDragonKiller.nodes.decisionNodes.yesBank.walkToBank.AtCaveEntrance;
import scripts.fluffeesRedDragonKiller.nodes.decisionNodes.yesBank.walkToBank.InCave;
import scripts.fluffeesRedDragonKiller.nodes.decisionNodes.yesBank.walkToBank.InFightPits;
import scripts.fluffeesRedDragonKiller.nodes.decisionNodes.yesBank.walkToBank.SeeFightPitEntrance;
import scripts.fluffeesRedDragonKiller.nodes.processNodes.EquipItems;
import scripts.fluffeesRedDragonKiller.nodes.processNodes.GrabEquipmentIDs;
import scripts.fluffeesRedDragonKiller.nodes.processNodes.PayToEnter;
import scripts.fluffeesRedDragonKiller.nodes.processNodes.banking.*;
import scripts.fluffeesRedDragonKiller.nodes.processNodes.combat.*;
import scripts.fluffeesRedDragonKiller.nodes.processNodes.navigation.fromRedDragons.FightPitsTeleport;
import scripts.fluffeesRedDragonKiller.nodes.processNodes.navigation.fromRedDragons.WaitForTeleport;
import scripts.fluffeesRedDragonKiller.nodes.processNodes.navigation.toRedDragons.LeaveCave;
import scripts.fluffeesRedDragonKiller.nodes.processNodes.navigation.toRedDragons.LeaveFightPits;
import scripts.fluffeesRedDragonKiller.nodes.processNodes.navigation.toRedDragons.brimhavenDungeon.*;
import scripts.fluffeesapi.client.ClientUtilities;
import scripts.fluffeesapi.client.cellrender.ColoredTextCellRenderer;
import scripts.fluffeesapi.client.clientextensions.ScriptExtension;
import scripts.fluffeesapi.client.wrappers.ColoredText;
import scripts.fluffeesapi.client.wrappers.Printer;
import scripts.fluffeesapi.scripting.antiban.AntiBanSingleton;
import scripts.fluffeesapi.scripting.frameworks.modulardecisiontree.nodes.ConstructorDecisionNode;
import scripts.fluffeesapi.scripting.frameworks.modulardecisiontree.nodes.ProcessNode;
import scripts.fluffeesapi.scripting.helpers.SkillsHelper;
import scripts.fluffeesapi.scripting.javafx.utilities.GUI;
import scripts.fluffeesapi.scripting.listeners.clickContinueListener.ClickContinueListener;
import scripts.fluffeesapi.scripting.minimalJson.Json;
import scripts.fluffeesapi.scripting.minimalJson.JsonObject;
import scripts.fluffeesapi.scripting.painting.fluffeesPublicPaint.FluffeesPaint;
import scripts.fluffeesapi.scripting.painting.fluffeesPublicPaint.PaintInfo;
import scripts.fluffeesapi.utilities.ArgumentUtilities;
import scripts.fluffeesapi.utilities.FileUtilities;
import scripts.fluffeesapi.utilities.Utilities;

import java.awt.*;
import java.io.*;
import java.net.MalformedURLException;
import java.util.HashMap;

@ScriptManifest(authors = {"Fluffee"}, category = "Money making", name = "Fluffee's RDK", version = 1.02, description = "Local version." +
        "\n" + "<html><br/><font color='red'>Version: 1.02</font></html>", gameMode = 1)

public class Main extends ScriptExtension implements Starting, Painting, PaintInfo, Ending, Arguments, MessageListening07 {

    private final Color PAINT_BACKGROUND = new Color(114, 10, 10);
    private final Color PAINT_BORDER = new Color(84, 0, 7);
    private Thread clickContinueListener = null;

    private final FluffeesPaint FLUFFEES_PAINT = new FluffeesPaint(this, FluffeesPaint.PaintLocations.TOP_RIGHT_CHATBOX, new Color[]{new Color(255, 255, 255)}, "Trebuchet MS", new Color[]{PAINT_BACKGROUND},
            new Color[]{PAINT_BORDER}, 1, false, 5, 3, 0);

    private double scriptVersion = getClass().getAnnotation(ScriptManifest.class).version();

    ProcessNode attack = new Attack();
    ProcessNode drinkPotion = new DrinkPotion();
    ProcessNode enableAutocast = new EnableAutocast();
    ProcessNode equipItems = new EquipItems();
    ProcessNode grabEquipmentIDs = new GrabEquipmentIDs();
    ProcessNode eat = new Eat();
    ProcessNode loot = new Loot();
    ProcessNode waitForKill = new WaitForKill();
    ProcessNode payToEnter = new PayToEnter();

    ProcessNode openBank = new OpenBank();
    ProcessNode closeBank = new CloseBank();
    ProcessNode bank = new Bank();
    ProcessNode waitForBankLoad = new WaitForBankLoad();
    ProcessNode walkToBank = new WalkToBank();

    ProcessNode fightPitsTeleport = new FightPitsTeleport();

    ProcessNode leaveCave = new LeaveCave();
    ProcessNode leaveFightPits = new LeaveFightPits();

    ProcessNode enterDungeon = new EnterDungeon();
    ProcessNode exitFirstRoom = new ExitFirstRoom();
    ProcessNode exitFourthRoom = new ExitFourthRoom();
    ProcessNode exitSecondRoom = new ExitSecondRoom();
    ProcessNode exitThirdRoom = new ExitThirdRoom();
    ProcessNode walkToDungeonEntrance = new WalkToDungeonEntrance();
    ProcessNode walkToSafespot = new WalkToSafespot();
    ProcessNode waitForTeleport = new WaitForTeleport();

    ConstructorDecisionNode shouldBank = new ShouldBank();
    ConstructorDecisionNode shouldEquipItems = new ShouldEquipItems();
    ConstructorDecisionNode shouldGrabIDs = new ShouldGrabIDs();
    ConstructorDecisionNode onKaramja = new OnKaramja();
    ConstructorDecisionNode atBank = new AtBank();
    ConstructorDecisionNode bankOpen = new BankOpen();
    ConstructorDecisionNode bankOpenEquip = new BankOpen();
    ConstructorDecisionNode bankLoaded = new BankLoaded();
    ConstructorDecisionNode seeFightPitsEntrance = new SeeFightPitEntrance();
    ConstructorDecisionNode inFightPits = new InFightPits();
    ConstructorDecisionNode inFightPitsBank = new InFightPits();
    ConstructorDecisionNode canTeleport = new CanTeleport();
    ConstructorDecisionNode inCave = new InCave();
    ConstructorDecisionNode atCaveEntrance = new AtCaveEntrance();

    ConstructorDecisionNode shouldEat = new ShouldEat();
    ConstructorDecisionNode isLooting = new IsLooting();
    ConstructorDecisionNode inSafespot = new InSafespot();
    ConstructorDecisionNode inCombat = new InCombat();
    ConstructorDecisionNode shouldDrinkPotion = new ShouldDrinkPotion();
    ConstructorDecisionNode needEnableAutocast = new NeedEnableAutocast();
    ConstructorDecisionNode inCombatBanking = new InCombat();
    ConstructorDecisionNode atRedDragons = new AtRedDragons();
    ConstructorDecisionNode atDungeonEntrance = new AtDungeonEntrance();
    ConstructorDecisionNode inBrimhavenDungeon = new InBrimhavenDungeon();
    ConstructorDecisionNode paidToEnterDungeon = new PaidToEnterDungeon();
    ConstructorDecisionNode inFirstRoom = new InFirstRoom();
    ConstructorDecisionNode inSecondRoom = new InSecondRoom();
    ConstructorDecisionNode inThirdRoom = new InThirdRoom();

    DecisionTree mainTree;

    private final String FXML_PATH = FileUtilities.createFile(FXML1.fxmlContents, "FluffeeScripts" + File.separator + "FluffeesRedDragonKiller" + File.separator + "fluffees-red-dragon-killer.fxml");
    private final String CSS_PATH = FileUtilities.createFile(CSSContents.cssContents, "FluffeeScripts" + File.separator + "FluffeesRedDragonKiller" + File.separator + "fluffees-red-dragon-killer.css");

    @Override
    public GUI getGUI() {
        try {
            return new GUI(new File(FXML_PATH).toURI().toURL(), new File(CSS_PATH).toURI().toURL(), false, "Fluffee's Red Dragon Killer");
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public void mainLoop() {
        FXML1.fxmlContents = null;
        GUIController.emptyCall();
        gui = null;
        while (!Variables.get().isStopScript()) {
            INode currentNode = mainTree.getValidNode();
            if (currentNode != null) {
                Variables.get().setScriptStatus(currentNode.getStatus());
                currentNode.execute();
            } else {
                Variables.get().setStopScript(true);
            }
            General.sleep(300);
        }
    }

    @Override
    public void onScriptStart() {
        Variables.get().setaCamera(super.aCamera);
        ClientUtilities.setCellRenderer(true, new FluffeesCellRenderer());
        AntiBanSingleton.get().setPrintDebug(true);
        AntiBanSingleton.get().setClientDebug(true);
        clickContinueListener = new Thread(new ClickContinueListener.ClickContinueBuilder().withCloseWorldMap().build());
        clickContinueListener.start();
        setInventoryObserverState(true);
        WebWalking.setUseAStar(true);
        while (Login.getLoginState() != Login.STATE.INGAME)
            General.sleep(200, 400);
        SkillsHelper.setStartSkills();
        setupNodes();
    }

    @Override
    public void onEnd() {
        super.onEnd();
        setInventoryObserverState(false);
        clickContinueListener.suspend();
        gui = null;
        Printer.println(ColoredText.criticalMessage("Script ending due to: " + Variables.get().getEndingReason()));

    }

    @Override
    public void onPaint(Graphics graphics) {
        FLUFFEES_PAINT.paint(graphics);
    }

    @Override
    public void passArguments(HashMap<String, String> hashMap) {
        HashMap<String, String> arguments = ArgumentUtilities.get(hashMap);
        if (arguments.size() == 0) {
            return;
        }
        String fileName = arguments.get("file");
        if (fileName == null) {
            Printer.println(ColoredText.criticalMessage("Error: You did not supply the arguments correctly, the format is: file=filename.json;"));
            return;
        }
        File settingsFile = new File(Util.getWorkingDirectory().toString() + File.separatorChar +"FluffeeScripts" + File.separatorChar + "FluffeesRedDragonKiller" + File.separatorChar + fileName);
        if (!settingsFile.exists()) {
            Printer.println(ColoredText.criticalMessage("Error: The file you supplied in the arguments does not exist."));
            return;
        }

        BufferedReader bufferedReader = null;
        try {
            bufferedReader = new BufferedReader(new FileReader(settingsFile));
            JsonObject jsonObject = Json.parse(bufferedReader).asObject();
            Variables.get().setAccountSettings(AccountSettings.parseJson(jsonObject));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            return;
        } catch (IOException e) {
            e.printStackTrace();
            return;
        }
        Printer.println(ColoredText.criticalMessage("Success, loading from arguments, skipping the GUI."));
        hasArguments = true;
    }

    public void setupNodes() {
        shouldEat.addOnTrueNode(eat);
        shouldEat.addOnFalseNode(shouldBank);
        shouldBank.addOnTrueNode(atBank);
        shouldBank.addOnFalseNode(shouldEquipItems);
        atBank.addOnTrueNode(bankOpen);
        atBank.addOnFalseNode(inFightPitsBank);
        shouldEquipItems.addOnTrueNode(bankOpenEquip);
        shouldEquipItems.addOnFalseNode(atRedDragons);
        bankOpenEquip.addOnTrueNode(closeBank);
        bankOpenEquip.addOnFalseNode(equipItems);
        shouldGrabIDs.addOnTrueNode(grabEquipmentIDs);
        shouldGrabIDs.addOnFalseNode(atRedDragons);
        atRedDragons.addOnTrueNode(isLooting);
        atRedDragons.addOnFalseNode(inFightPits);
        bankOpen.addOnTrueNode(bankLoaded);
        bankOpen.addOnFalseNode(openBank);
        inFightPitsBank.addOnTrueNode(walkToBank);
        inFightPitsBank.addOnFalseNode(canTeleport);
        canTeleport.addOnTrueNode(fightPitsTeleport);
        canTeleport.addOnFalseNode(inCombatBanking);
        inCombatBanking.addOnTrueNode(walkToSafespot);
        inCombatBanking.addOnFalseNode(waitForTeleport); //TODO: Write method to walk out of dungeon
        isLooting.addOnTrueNode(loot);
        isLooting.addOnFalseNode(inSafespot);
        inFightPits.addOnTrueNode(leaveFightPits);
        inFightPits.addOnFalseNode(inCave);
        bankLoaded.addOnTrueNode(bank);
        bankLoaded.addOnFalseNode(waitForBankLoad);
        inSafespot.addOnTrueNode(inCombat);
        inSafespot.addOnFalseNode(walkToSafespot);
        inCave.addOnTrueNode(leaveCave);
        inCave.addOnFalseNode(inBrimhavenDungeon);
        inCombat.addOnTrueNode(waitForKill);
        inCombat.addOnFalseNode(needEnableAutocast);
        inBrimhavenDungeon.addOnTrueNode(inFirstRoom);
        inBrimhavenDungeon.addOnFalseNode(atDungeonEntrance);
        needEnableAutocast.addOnTrueNode(enableAutocast);
        needEnableAutocast.addOnFalseNode(shouldDrinkPotion);
        inFirstRoom.addOnTrueNode(exitFirstRoom);
        inFirstRoom.addOnFalseNode(inSecondRoom);
        atDungeonEntrance.addOnTrueNode(paidToEnterDungeon);
        atDungeonEntrance.addOnFalseNode(walkToDungeonEntrance);
        shouldDrinkPotion.addOnTrueNode(drinkPotion);
        shouldDrinkPotion.addOnFalseNode(attack);
        inSecondRoom.addOnTrueNode(exitSecondRoom);
        inSecondRoom.addOnFalseNode(inThirdRoom);
        paidToEnterDungeon.addOnTrueNode(enterDungeon);
        paidToEnterDungeon.addOnFalseNode(payToEnter);
        inThirdRoom.addOnTrueNode(exitThirdRoom);
        inThirdRoom.addOnFalseNode(exitFourthRoom);
        mainTree = new DecisionTree(shouldEat);
    }

    @Override
    public void inventoryItemGained(int id, int count) {
        for (Interactable equipment : Variables.get().getAccountSettings().getEquipment()) {
            if (equipment.getId() == id)
                return;
        }
        Variables.get().setLootGained(true);
        Variables.get().addToProfit(id, count);
    }

    @Override
    public void inventoryItemLost(int id, int count) {
        for (Interactable equipment : Variables.get().getAccountSettings().getEquipment()) {
            if (equipment.getId() == id)
                return;
        }
        Variables.get().removeFromProfit(id, count);
    }

    @Override
    public String[] getPaintInfo() {
        return new String[]{"Fluffee's Red Dragon Killer v" + String.format("%.2f", scriptVersion), "Runtime: " + Timing.msToString(this.getRunningTime()), "Status: " + (AntiBanSingleton.get().isSleeping() ? "Waiting for ABC2 Sleep to End" : Variables.get().getScriptStatus()),
                "Profit (GP/H): " + Variables.get().getProfit() + " (" + Utilities.getAmountPerHour(Variables.get().getProfit(), this.getRunningTime()) + ")",
                SkillsHelper.getPrettySkillName(SkillsHelper.getSkillWithMostIncrease(Skills.SKILLS.HITPOINTS)) + " XP (P/H): " + SkillsHelper.getReceivedXP(SkillsHelper.getSkillWithMostIncrease(Skills.SKILLS.HITPOINTS)) +
                        " (" + Utilities.getAmountPerHour(SkillsHelper.getReceivedXP(SkillsHelper.getSkillWithMostIncrease(Skills.SKILLS.HITPOINTS)), this.getRunningTime()) + ")"};
    }

    @Override
    public void playerMessageReceived(String s, String s1) {

    }

    @Override
    public void clanMessageReceived(String s, String s1) {

    }

    @Override
    public void tradeRequestReceived(String s) {

    }

    @Override
    public void personalMessageReceived(String s, String s1) {

    }

    @Override
    public void duelRequestReceived(String s, String s1) {

    }

    @Override
    public void serverMessageReceived(String message) {
        if (message.startsWith("You must wait another")) {
            int waitTime = Integer.parseInt(message.replaceAll("[^-?0-9]+", " ").trim());
            Variables.get().setLastTeleportTime(System.currentTimeMillis()-((20-waitTime) * 60 * 1000));
        }
    }

    @Override
    public void onStart() {
        Printer.initialize(ClientUtilities.getJList(true));
        ClientUtilities.setCellRenderer(true, new ColoredTextCellRenderer());
    }
}
