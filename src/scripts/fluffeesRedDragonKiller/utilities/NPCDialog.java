package scripts.fluffeesRedDragonKiller.utilities;

import org.tribot.api.General;
import scripts.fluffeesRedDragonKiller.data.Constants;
import scripts.fluffeesRedDragonKiller.data.Variables;
import scripts.fluffeesapi.scripting.antiban.AntiBanSingleton;
import scripts.fluffeesapi.utilities.Utilities;
import scripts.fluffeesapi.utilities.npc.NPCInteraction;

public class NPCDialog {

    public static boolean handleNPCChat(String npcName, String interactionOption) {
        return handleNPCChat(npcName, interactionOption, false);
    }

    public static boolean handleNPCChat(String npcName, String interactionOption, boolean ranged) {
        if (!scripts.fluffeesapi.client.wrappers.clientWrappers.NPCChat.inChat(3)) {
            NPCInteraction.interactABC2(interactionOption, ranged, npcName);
        }
        while (scripts.fluffeesapi.client.wrappers.clientWrappers.NPCChat.inChat(3)) {
            if (Variables.get().getAccountSettings().getClickContinue() != Constants.HOLD_SPACEBAR) {
                General.sleep(scripts.fluffeesapi.client.wrappers.clientWrappers.NPCChat.getReadWaitTime(scripts.fluffeesapi.client.wrappers.clientWrappers.NPCChat.getMessage()));
                scripts.fluffeesapi.client.wrappers.clientWrappers.NPCChat.clickContinue(true, Variables.get().getAccountSettings().getClickContinue() == Constants.TAP_SPACEBAR);
            } else {
                long startTime = System.currentTimeMillis();
                scripts.fluffeesapi.client.wrappers.clientWrappers.NPCChat.holdSpaceForChat();
                Variables.get().getAccountSettings().setChatABC2Counter(Variables.get().getAccountSettings().getChatABC2Counter() + 1);
                Variables.get().getAccountSettings().setChatABC2Sleep(Utilities.addWaitTime((int) (System.currentTimeMillis() - startTime), Variables.get().getAccountSettings().getChatABC2Counter(),
                        Variables.get().getAccountSettings().getChatABC2Sleep()));
                AntiBanSingleton.get().setLastReactionTime(Variables.get().getAccountSettings().getChatABC2Sleep());
                AntiBanSingleton.get().generateSupportingTrackerInfo(Variables.get().getAccountSettings().getChatABC2Sleep(), false);
                AntiBanSingleton.get().sleepReactionTime();
            }
        }
        return !scripts.fluffeesapi.client.wrappers.clientWrappers.NPCChat.inChat(3);
    }
}
