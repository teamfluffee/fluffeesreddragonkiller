package scripts.fluffeesRedDragonKiller.nodes.processNodes;

import org.tribot.api2007.types.RSItem;
import scripts.fluffeesRedDragonKiller.data.Variables;
import scripts.fluffeesapi.client.wrappers.clientWrappers.InventoryWrapper;
import scripts.fluffeesapi.scripting.frameworks.decisionTree.decisionTreeFramework.ProcessNode;

public class GrabEquipmentIDs extends ProcessNode {
    @Override
    public String getStatus() {
        return "Grabbing Missing IDs.";
    }

    @Override
    public void execute() {
        RSItem item = null;
        for (int i = 0; i < Variables.get().getAccountSettings().getEquipment().length; i++) {
            if (Variables.get().getAccountSettings().getEquipment()[i].isAllIdsFound())
                continue;
            item = Variables.get().getAccountSettings().getEquipment()[i].getSlot().getItem();
            if (item == null)
                Variables.get().getAccountSettings().getEquipment()[i].addInteractableID(item.getID());
        }
        if (Variables.get().getAccountSettings().isUseRanged()) {
            if (!Variables.get().getAccountSettings().getAmmo().isAllIdsFound()) {
                item = Variables.get().getAccountSettings().getAmmo().getSlot().getItem();
                if (item != null)
                    Variables.get().getAccountSettings().getAmmo().addInteractableID(item.getID());
            }
        } else {
            for (int i = 0; i < Variables.get().getAccountSettings().getSpell().getRunesRequired().length; i++) {
                if (Variables.get().getAccountSettings().getSpell().getRunesRequired()[i].isAllIdsFound())
                    continue;
                item = InventoryWrapper.findFirst(Variables.get().getAccountSettings().getSpell().getRunesRequired()[i].getName());
                if (item == null)
                    Variables.get().getAccountSettings().getSpell().getRunesRequired()[i].addInteractableID(item.getID());
            }
        }
        if (Variables.get().getAccountSettings().isUsePotions() && !Variables.get().getAccountSettings().getPotion().isAllIdsFound()) {
            for (int i = 1; i <= Variables.get().getAccountSettings().getPotion().getDefaultDose().getDoseValue(); i++) {
                RSItem potion = InventoryWrapper.findFirst(Variables.get().getAccountSettings().getPotion().getPotionName(i));
                if (potion == null)
                    continue;
                Variables.get().getAccountSettings().getPotion().setPotionID(i, potion.getID());
            }
        }
        if (!Variables.get().getAccountSettings().getAxe().isAllIdsFound()) {
            item = InventoryWrapper.findFirst(Variables.get().getAccountSettings().getAxe().getName());
            if (item != null)
                Variables.get().getAccountSettings().getAxe().addInteractableID(item.getID());
        }
        if (!Variables.get().getAccountSettings().getFood().isAllIdsFound()) {
            item = InventoryWrapper.findFirst(Variables.get().getAccountSettings().getFood().getName());
            if (item != null)
                Variables.get().getAccountSettings().getFood().addInteractableID(item.getID());
        }
    }
}
