package scripts.fluffeesRedDragonKiller.nodes.processNodes;

import org.tribot.api.General;
import org.tribot.api.Timing;
import org.tribot.api2007.NPCs;
import org.tribot.api2007.WebWalking;
import org.tribot.api2007.types.RSNPC;
import scripts.fluffeesRedDragonKiller.data.Constants;
import scripts.fluffeesRedDragonKiller.data.Variables;
import scripts.fluffeesRedDragonKiller.utilities.NPCDialog;
import scripts.fluffeesapi.scripting.frameworks.decisionTree.decisionTreeFramework.ProcessNode;
import scripts.fluffeesapi.scripting.types.interactables.Interactable;
import scripts.fluffeesapi.utilities.Conditions;

public class PayToEnter extends ProcessNode {
    @Override
    public String getStatus() {
        return "Paying to enter Dungeon";
    }

    @Override
    public void execute() {
        RSNPC[] saniboch;
        if (Variables.get().getSaniboch().getId() == Interactable.DEFAULT_ID)
            saniboch = NPCs.find(Variables.get().getSaniboch().getName());
        else
            saniboch = NPCs.find(Variables.get().getSaniboch().getId());

        if (saniboch.length < 1) {
            WebWalking.walkTo(Constants.DUNGEON_ENTRANCE);
            Timing.waitCondition(Conditions.nearTile(2, Constants.DUNGEON_ENTRANCE), General.random(3000, 5000));
            return;
        }
        if (Variables.get().getSaniboch().getId() == Interactable.DEFAULT_ID)
            Variables.get().getSaniboch().addInteractableID(saniboch[0].getID());

        NPCDialog.handleNPCChat(Variables.get().getSaniboch().getName(), Variables.get().getSaniboch().getAction());
    }
}
