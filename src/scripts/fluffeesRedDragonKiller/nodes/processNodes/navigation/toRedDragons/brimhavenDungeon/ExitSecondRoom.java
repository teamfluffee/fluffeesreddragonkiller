package scripts.fluffeesRedDragonKiller.nodes.processNodes.navigation.toRedDragons.brimhavenDungeon;

import org.tribot.api.Clicking;
import org.tribot.api.General;
import org.tribot.api.Timing;
import org.tribot.api2007.Objects;
import org.tribot.api2007.Player;
import org.tribot.api2007.WebWalking;
import org.tribot.api2007.types.RSObject;
import scripts.fluffeesRedDragonKiller.data.Constants;
import scripts.fluffeesRedDragonKiller.data.Variables;
import scripts.fluffeesapi.scripting.frameworks.decisionTree.decisionTreeFramework.ProcessNode;
import scripts.fluffeesapi.scripting.types.interactables.Interactable;
import scripts.fluffeesapi.utilities.Conditions;

import java.util.function.BooleanSupplier;

public class ExitSecondRoom extends ProcessNode {
    @Override
    public String getStatus() {
        return "Exiting Second Room";
    }

    @Override
    public void execute() {
        RSObject[] steppingStones;
        if (Variables.get().getEnterSteppingStone().getId() == Interactable.DEFAULT_ID)
            steppingStones = Objects.findNearest(5, Variables.get().getEnterSteppingStone().getName());
        else
            steppingStones = Objects.findNearest(5, Variables.get().getEnterSteppingStone().getId());

        if (steppingStones.length < 1) {
            WebWalking.walkTo(Constants.SECOND_ROOM_STEPPING_STONES_TILE);
            Timing.waitCondition(Conditions.nearTile(5, Constants.SECOND_ROOM_STEPPING_STONES_TILE), General.random(3000, 5000));
            return;
        }

        if (Variables.get().getEnterSteppingStone().getId() == Interactable.DEFAULT_ID)
            Variables.get().getEnterSteppingStone().addInteractableID(steppingStones[0].getID());

        if (!steppingStones[0].isOnScreen() || !steppingStones[0].isClickable())
            Variables.get().getaCamera().attemptAimCamera(steppingStones[0]);

        if (Clicking.click(Variables.get().getEnterSteppingStone().getAction(), steppingStones[0]))
            if (Timing.waitCondition(Conditions.startedAnimation(741), General.random(3000, 5000)))
                Timing.waitCondition(new BooleanSupplier() {
                    @Override
                    public boolean getAsBoolean() {
                        General.sleep(200, 400);
                        return Constants.BRIMHAVEN_DUNGEON_THIRD_ROOM.contains(Player.getPosition());
                    }
                }, General.random(7000, 10000));
    }
}
