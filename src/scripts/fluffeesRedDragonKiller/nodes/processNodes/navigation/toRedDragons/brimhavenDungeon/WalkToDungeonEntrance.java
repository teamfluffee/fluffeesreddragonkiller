package scripts.fluffeesRedDragonKiller.nodes.processNodes.navigation.toRedDragons.brimhavenDungeon;

import org.tribot.api.General;
import org.tribot.api.Timing;
import org.tribot.api2007.Objects;
import org.tribot.api2007.WebWalking;
import org.tribot.api2007.types.RSObject;
import scripts.fluffeesRedDragonKiller.data.Constants;
import scripts.fluffeesRedDragonKiller.data.Variables;
import scripts.fluffeesapi.scripting.frameworks.decisionTree.decisionTreeFramework.ProcessNode;
import scripts.fluffeesapi.scripting.types.interactables.Interactable;
import scripts.fluffeesapi.utilities.Conditions;

public class WalkToDungeonEntrance extends ProcessNode {
    @Override
    public String getStatus() {
        return "Walking to Brimhaven Dungeon Entrance";
    }

    @Override
    public void execute() {
        RSObject[] dungeonEntrances;
        if (Variables.get().getDungeonEntrance().getId() == Interactable.DEFAULT_ID)
            dungeonEntrances = Objects.findNearest(5, Variables.get().getDungeonEntrance().getName());
        else
            dungeonEntrances = Objects.findNearest(5, Variables.get().getDungeonEntrance().getId());
        if (dungeonEntrances.length < 1) {
            WebWalking.walkTo(Constants.DUNGEON_ENTRANCE);
            Timing.waitCondition(Conditions.nearTile(2, Constants.DUNGEON_ENTRANCE), General.random(8000, 10000));
            return;
        }

        if (Variables.get().getDungeonEntrance().getId() == Interactable.DEFAULT_ID)
            Variables.get().getDungeonEntrance().addInteractableID(dungeonEntrances[0].getID());
    }
}
