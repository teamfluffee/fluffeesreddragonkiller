package scripts.fluffeesRedDragonKiller.nodes.processNodes.navigation.toRedDragons.brimhavenDungeon;

import org.tribot.api.Clicking;
import org.tribot.api.General;
import org.tribot.api.Timing;
import org.tribot.api2007.Objects;
import org.tribot.api2007.Player;
import org.tribot.api2007.Walking;
import org.tribot.api2007.WebWalking;
import org.tribot.api2007.ext.Filters;
import org.tribot.api2007.types.RSObject;
import scripts.fluffeesRedDragonKiller.data.Constants;
import scripts.fluffeesRedDragonKiller.data.Variables;
import scripts.fluffeesapi.scripting.frameworks.decisionTree.decisionTreeFramework.ProcessNode;
import scripts.fluffeesapi.scripting.types.interactables.Interactable;
import scripts.fluffeesapi.utilities.Conditions;

import java.util.function.BooleanSupplier;

public class ExitThirdRoom extends ProcessNode {

    @Override
    public String getStatus() {
        return "Exiting Third Room";
    }

    @Override
    public void execute() {
        RSObject[] vines;
        if (Variables.get().getThirdVines().getId() == Interactable.DEFAULT_ID)
            vines = Objects.find(5, Filters.Objects.tileEquals(Constants.THIRD_ROOM_VINES_TILE).combine(Filters.Objects.nameEquals(Variables.get().getThirdVines().getName()), false));
        else
            vines = Objects.find(5, Filters.Objects.tileEquals(Constants.THIRD_ROOM_VINES_TILE).combine(Filters.Objects.idEquals(Variables.get().getThirdVines().getId()), false));

        if (vines.length < 1) {
            if (Constants.BRIMHAVEN_DUNGEON_THIRD_ROOM_PART_1.contains(Player.getPosition())) {
                WebWalking.walkTo(Constants.THIRD_ROOM_FIRST_TILE);
                Timing.waitCondition(Conditions.nearTile(5, Constants.THIRD_ROOM_FIRST_TILE), General.random(3000, 5000));
            }
            if (Constants.BRIMHAVEN_DUNGEON_THIRD_ROOM_PART_2.contains(Player.getPosition())) {
                Walking.blindWalkTo(Constants.THIRD_ROOM_SECOND_TILE);
                Timing.waitCondition(Conditions.nearTile(5, Constants.THIRD_ROOM_SECOND_TILE), General.random(3000, 5000));
            }
            if (Constants.BRIMHAVEN_DUNGEON_THIRD_ROOM_PART_3.contains(Player.getPosition())) {
                WebWalking.walkTo(Constants.THIRD_ROOM_THIRD_TILE);
                Timing.waitCondition(Conditions.nearTile(5, Constants.THIRD_ROOM_THIRD_TILE), General.random(3000, 5000));
            }
            if (Constants.BRIMHAVEN_DUNGEON_THIRD_ROOM_PART_4.contains(Player.getPosition())) {
                WebWalking.walkTo(Constants.THIRD_ROOM_VINES_TILE);
                Timing.waitCondition(Conditions.nearTile(5, Constants.THIRD_ROOM_VINES_TILE), General.random(3000, 5000));
            }
            return;
        }

        if (Variables.get().getThirdVines().getId() == Interactable.DEFAULT_ID)
            Variables.get().getThirdVines().addInteractableID(vines[0].getID());

        if (!vines[0].isOnScreen() || !vines[0].isClickable())
            Variables.get().getaCamera().attemptAimCamera(vines[0]);

        if (Clicking.click(Variables.get().getThirdVines().getAction(), vines[0]))
            if (Timing.waitCondition(Conditions.startedAnimating(), General.random(3000, 5000)))
                Timing.waitCondition(new BooleanSupplier() {
                    @Override
                    public boolean getAsBoolean() {
                        General.sleep(200, 400);
                        return !Constants.BRIMHAVEN_DUNGEON_THIRD_ROOM.contains(Player.getPosition());
                    }
                }, General.random(7000, 10000));
    }
}