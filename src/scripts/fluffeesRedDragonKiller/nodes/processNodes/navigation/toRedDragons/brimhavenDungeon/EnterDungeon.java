package scripts.fluffeesRedDragonKiller.nodes.processNodes.navigation.toRedDragons.brimhavenDungeon;

import org.tribot.api.Clicking;
import org.tribot.api.General;
import org.tribot.api.Timing;
import org.tribot.api2007.Objects;
import org.tribot.api2007.WebWalking;
import org.tribot.api2007.types.RSObject;
import scripts.fluffeesRedDragonKiller.data.Constants;
import scripts.fluffeesRedDragonKiller.data.Variables;
import scripts.fluffeesapi.scripting.frameworks.decisionTree.decisionTreeFramework.ProcessNode;
import scripts.fluffeesapi.scripting.types.interactables.Interactable;
import scripts.fluffeesapi.utilities.Conditions;

public class EnterDungeon extends ProcessNode {
    @Override
    public String getStatus() {
        return "Entering Dungeon";
    }

    @Override
    public void execute() {
        RSObject[] dungeonEntrances;
        if (Variables.get().getDungeonEntrance().getId() == Interactable.DEFAULT_ID)
            dungeonEntrances = Objects.findNearest(5, Variables.get().getDungeonEntrance().getName());
        else
            dungeonEntrances = Objects.findNearest(5, Variables.get().getDungeonEntrance().getId());
        if (dungeonEntrances.length < 1) {
            WebWalking.walkTo(Constants.DUNGEON_ENTRANCE);
            Timing.waitCondition(Conditions.nearTile(2, Constants.DUNGEON_ENTRANCE), General.random(8000, 10000));
            return;
        }

        if (Variables.get().getDungeonEntrance().getId() == Interactable.DEFAULT_ID)
            Variables.get().getDungeonEntrance().addInteractableID(dungeonEntrances[0].getID());

        if (!dungeonEntrances[0].isOnScreen() || !dungeonEntrances[0].isClickable())
            Variables.get().getaCamera().attemptAimCamera(dungeonEntrances[0]);

        Clicking.click(Variables.get().getDungeonEntrance().getAction(), dungeonEntrances[0]);
        Timing.waitCondition(Conditions.areaContains(Constants.BRIMHAVEN_DUNGEON), General.random(5000, 10000));
    }
}
