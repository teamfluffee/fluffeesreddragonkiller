package scripts.fluffeesRedDragonKiller.nodes.processNodes.navigation.toRedDragons.brimhavenDungeon;

import org.tribot.api.General;
import org.tribot.api.Timing;
import org.tribot.api2007.Player;
import org.tribot.api2007.WebWalking;
import scripts.fluffeesRedDragonKiller.data.Constants;
import scripts.fluffeesRedDragonKiller.data.Variables;
import scripts.fluffeesapi.scripting.frameworks.decisionTree.decisionTreeFramework.ProcessNode;
import scripts.fluffeesapi.utilities.Conditions;

public class WalkToSafespot extends ProcessNode {
    @Override
    public String getStatus() {
        return "Walking to Safespot";
    }

    @Override
    public void execute() {
        WebWalking.walkTo(Constants.RED_DRAGON_SAFESPOT);
        Timing.waitCondition(Conditions.stoppedMoving(), General.random(3000, 5000));
        if (!Player.getPosition().equals(Constants.RED_DRAGON_SAFESPOT)) {
            if (!Constants.RED_DRAGON_SAFESPOT.isOnScreen())
                Variables.get().getaCamera().turnToTile(Constants.RED_DRAGON_SAFESPOT);
            Constants.RED_DRAGON_SAFESPOT.click();
            Timing.waitCondition(Conditions.nearTile(0, Constants.RED_DRAGON_SAFESPOT), General.random(1000, 3000));
        }
    }
}
