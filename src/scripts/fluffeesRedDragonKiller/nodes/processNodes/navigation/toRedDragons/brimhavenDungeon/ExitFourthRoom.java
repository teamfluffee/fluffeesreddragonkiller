package scripts.fluffeesRedDragonKiller.nodes.processNodes.navigation.toRedDragons.brimhavenDungeon;

import org.tribot.api.Clicking;
import org.tribot.api.General;
import org.tribot.api.Timing;
import org.tribot.api2007.Objects;
import org.tribot.api2007.Player;
import org.tribot.api2007.WebWalking;
import org.tribot.api2007.types.RSObject;
import scripts.fluffeesRedDragonKiller.data.Constants;
import scripts.fluffeesRedDragonKiller.data.Variables;
import scripts.fluffeesapi.scripting.frameworks.decisionTree.decisionTreeFramework.ProcessNode;
import scripts.fluffeesapi.scripting.types.interactables.Interactable;
import scripts.fluffeesapi.utilities.Conditions;

import java.util.function.BooleanSupplier;

public class ExitFourthRoom extends ProcessNode {

    @Override
    public String getStatus() {
        return "Exiting Fourth Room";
    }

    @Override
    public void execute() {
        RSObject[] vines;
        if (Variables.get().getFourthLogBalanceExit().getId() == Interactable.DEFAULT_ID)
            vines = Objects.find(5, Variables.get().getFourthLogBalanceExit().getName());
        else
            vines = Objects.find(5, Variables.get().getFourthLogBalanceExit().getId());

        if (vines.length < 1) {
            WebWalking.walkTo(Constants.FOURTH_ROOM_LOG_BALANCE_TILE);
            Timing.waitCondition(Conditions.nearTile(5, Constants.FOURTH_ROOM_LOG_BALANCE_TILE), General.random(3000, 5000));
            return;
        }

        if (Variables.get().getFourthLogBalanceExit().getId() == Interactable.DEFAULT_ID)
            Variables.get().getFourthLogBalanceExit().addInteractableID(vines[0].getID());

        if (!vines[0].isOnScreen() || !vines[0].isClickable())
            Variables.get().getaCamera().attemptAimCamera(vines[0]);

        if (Clicking.click(Variables.get().getFourthLogBalanceExit().getAction(), vines[0]))
            Timing.waitCondition(new BooleanSupplier() {
                @Override
                public boolean getAsBoolean() {
                    General.sleep(200, 400);
                    return !Constants.BRIMHAVEN_DUNGEON_FOURTH_ROOM.contains(Player.getPosition());
                }
            }, General.random(7000, 10000));
    }
}