package scripts.fluffeesRedDragonKiller.nodes.processNodes.navigation.toRedDragons;

import org.tribot.api.Clicking;
import org.tribot.api.General;
import org.tribot.api.Timing;
import org.tribot.api2007.Objects;
import org.tribot.api2007.Player;
import org.tribot.api2007.WebWalking;
import org.tribot.api2007.types.RSObject;
import scripts.fluffeesRedDragonKiller.data.Constants;
import scripts.fluffeesRedDragonKiller.data.Variables;
import scripts.fluffeesapi.scripting.frameworks.decisionTree.decisionTreeFramework.ProcessNode;
import scripts.fluffeesapi.scripting.types.interactables.Interactable;
import scripts.fluffeesapi.utilities.Conditions;

import java.util.function.BooleanSupplier;

public class LeaveCave extends ProcessNode {
    @Override
    public String getStatus() {
        return "Leaving Karamja Cave";
    }

    @Override
    public void execute() {
        RSObject[] caveExits;
        if (Variables.get().getKaramjaCaveExit().getId() == Interactable.DEFAULT_ID)
            caveExits = Objects.findNearest(10, Variables.get().getKaramjaCaveExit().getName());
        else
            caveExits = Objects.findNearest(10, Variables.get().getKaramjaCaveExit().getId());
        if (caveExits.length < 1) {
            WebWalking.walkTo(Constants.KARAMJA_CAVE_ROPE);
            Timing.waitCondition(Conditions.nearTile(2, Constants.KARAMJA_CAVE_ROPE), General.random(8000, 10000));
            return;
        }

        if (Variables.get().getKaramjaCaveExit().getId() == Interactable.DEFAULT_ID)
            Variables.get().getKaramjaCaveExit().addInteractableID(caveExits[0].getID());

        if (!caveExits[0].isOnScreen() || !caveExits[0].isClickable())
            Variables.get().getaCamera().attemptAimCamera(caveExits[0]);

        if (Clicking.click(Variables.get().getKaramjaCaveExit().getAction(), caveExits[0])) {
            Timing.waitCondition(new BooleanSupplier() {
                @Override
                public boolean getAsBoolean() {
                    General.sleep(200, 400);
                    return !Constants.KARAMJA_CAVE.contains(Player.getPosition());
                }
            }, General.random(7000, 10000));
        }
    }
}
