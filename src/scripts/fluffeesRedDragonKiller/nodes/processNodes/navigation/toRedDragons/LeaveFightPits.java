package scripts.fluffeesRedDragonKiller.nodes.processNodes.navigation.toRedDragons;

import org.tribot.api.Clicking;
import org.tribot.api.General;
import org.tribot.api.Timing;
import org.tribot.api2007.Objects;
import org.tribot.api2007.WebWalking;
import org.tribot.api2007.types.RSObject;
import org.tribot.api2007.types.RSVarBit;
import scripts.fluffeesRedDragonKiller.data.Constants;
import scripts.fluffeesRedDragonKiller.data.Variables;
import scripts.fluffeesapi.scripting.frameworks.decisionTree.decisionTreeFramework.ProcessNode;
import scripts.fluffeesapi.scripting.types.interactables.Interactable;
import scripts.fluffeesapi.utilities.Conditions;

import java.util.function.BooleanSupplier;

public class LeaveFightPits extends ProcessNode {
    @Override
    public String getStatus() {
        return "Leaving Fight Pits";
    }

    @Override
    public void execute() {
        RSObject[] caveExits;
        if (Variables.get().getFightPitsExit().getId() == Interactable.DEFAULT_ID)
            caveExits = Objects.findNearest(5, Variables.get().getFightPitsExit().getName());
        else
            caveExits = Objects.findNearest(5, Variables.get().getFightPitsExit().getId());
        if (caveExits.length < 1) {
            WebWalking.walkTo(Constants.FIGHT_PITS_ENTRANCE);
            Timing.waitCondition(Conditions.nearTile(2, Constants.FIGHT_PITS_ENTRANCE), General.random(8000, 10000));
            return;
        }

        if (Variables.get().getFightPitsExit().getId() == Interactable.DEFAULT_ID)
            Variables.get().getFightPitsExit().addInteractableID(caveExits[0].getID());

        if (!caveExits[0].isOnScreen() || !caveExits[0].isClickable())
            caveExits[0].adjustCameraTo();
        Clicking.click(Variables.get().getFightPitsExit().getAction(), caveExits[0]);
        Timing.waitCondition(new BooleanSupplier() {
            @Override
            public boolean getAsBoolean() {
                General.sleep(200, 400);
                return RSVarBit.get(Constants.FIGHT_PITS_VARBIT).getValue() == 0;
            }
        }, General.random(7000, 10000));
    }
}
