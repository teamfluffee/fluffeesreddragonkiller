package scripts.fluffeesRedDragonKiller.nodes.processNodes.navigation.fromRedDragons;

import org.tribot.api.General;
import org.tribot.api.Timing;
import scripts.fluffeesRedDragonKiller.data.Variables;
import scripts.fluffeesapi.game.minigames.MinigameUtilities;
import scripts.fluffeesapi.scripting.frameworks.decisionTree.decisionTreeFramework.ProcessNode;
import scripts.fluffeesapi.utilities.Conditions;

public class FightPitsTeleport extends ProcessNode {
    @Override
    public String getStatus() {
        return "Teleporting to Fight Pits";
    }

    @Override
    public void execute() {
        MinigameUtilities.castMinigameTeleport(MinigameUtilities.MINIGAMES.TZHAAR_FIGHT_PIT);
        if (Timing.waitCondition(Conditions.startedAnimation(MinigameUtilities.MINIGAME_TELEPORT_ANIMATION), General.random(3000, 5000)))
            if (Timing.waitCondition(Conditions.stoppedAnimating(), General.random(15000, 20000)))
                Variables.get().setLastTeleportTime(System.currentTimeMillis());
    }
}
