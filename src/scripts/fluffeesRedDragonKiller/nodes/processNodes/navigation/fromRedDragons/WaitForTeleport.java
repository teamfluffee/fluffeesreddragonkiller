package scripts.fluffeesRedDragonKiller.nodes.processNodes.navigation.fromRedDragons;

import org.tribot.api.General;
import scripts.fluffeesRedDragonKiller.data.Variables;
import scripts.fluffeesapi.scripting.antiban.AntiBanSingleton;
import scripts.fluffeesapi.scripting.frameworks.decisionTree.decisionTreeFramework.ProcessNode;

public class WaitForTeleport extends ProcessNode {
    @Override
    public String getStatus() {
        return "Waiting for Teleport Availability";
    }

    @Override
    public void execute() {
        int waitTime = Variables.get().getTimeToTeleport();
        int temp = waitTime;
        while (temp > 0) {
            AntiBanSingleton.get().resolveTimedActions();
            int sleep = General.random(2000, 5000);
            temp -= sleep;
            General.sleep(sleep);
        }
        AntiBanSingleton.get().generateSupportingTrackerInfo(waitTime - temp, true);
        AntiBanSingleton.get().sleepReactionTime();
    }
}
