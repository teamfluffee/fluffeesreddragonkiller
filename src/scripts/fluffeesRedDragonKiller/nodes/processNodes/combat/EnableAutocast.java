package scripts.fluffeesRedDragonKiller.nodes.processNodes.combat;

import scripts.fluffeesRedDragonKiller.data.Variables;
import scripts.fluffeesapi.game.magic.Autocast;
import scripts.fluffeesapi.scripting.frameworks.decisionTree.decisionTreeFramework.ProcessNode;

public class EnableAutocast extends ProcessNode {
    @Override
    public String getStatus() {
        return "Enabling Autocast";
    }

    @Override
    public void execute() {
        Autocast.enableAutocast(Variables.get().getAccountSettings().getSpell(), false);
    }
}
