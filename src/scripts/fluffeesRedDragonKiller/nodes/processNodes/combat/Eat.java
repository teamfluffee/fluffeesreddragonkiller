package scripts.fluffeesRedDragonKiller.nodes.processNodes.combat;

import org.tribot.api.General;
import org.tribot.api2007.Combat;
import org.tribot.api2007.Inventory;
import org.tribot.api2007.Skills;
import org.tribot.api2007.types.RSItem;
import scripts.fluffeesRedDragonKiller.data.Variables;
import scripts.fluffeesapi.scripting.frameworks.decisionTree.decisionTreeFramework.ProcessNode;
import scripts.fluffeesapi.utilities.CombatUtilities;

public class Eat extends ProcessNode {
    @Override
    public String getStatus() {
        return "Eating food";
    }

    @Override
    public void execute() {
        RSItem[] food;
        if (Variables.get().getAccountSettings().getFood().isAllIdsFound())
            food = Inventory.find(Variables.get().getAccountSettings().getFood().getInteractableIDs());
        else
            food = Inventory.find(Variables.get().getAccountSettings().getFood().getName());
        if (food.length < 1)
            //TODO: Out of food
            return;
        Variables.get().getAccountSettings().getFood().addInteractableID(food[0].getID());
        int targetHitpoints = (int) (Math.ceil(Skills.getActualLevel(Skills.SKILLS.HITPOINTS) * (General.random(90, 100)/100.0)));
        for (int i = 0; i < food.length && Combat.getHPRatio() < targetHitpoints; i++) {
            CombatUtilities.eat(food[i], Variables.get().getAccountSettings().getFood().getAction());
        }
    }
}
