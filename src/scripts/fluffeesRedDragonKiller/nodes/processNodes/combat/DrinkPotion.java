package scripts.fluffeesRedDragonKiller.nodes.processNodes.combat;

import org.tribot.api.Clicking;
import org.tribot.api.General;
import org.tribot.api.Timing;
import org.tribot.api2007.types.RSItem;
import scripts.fluffeesRedDragonKiller.data.Variables;
import scripts.fluffeesapi.client.wrappers.clientWrappers.InventoryWrapper;
import scripts.fluffeesapi.scripting.frameworks.decisionTree.decisionTreeFramework.ProcessNode;
import scripts.fluffeesapi.utilities.Conditions;

public class DrinkPotion extends ProcessNode {
    @Override
    public String getStatus() {
        return "Drinking potion";
    }

    @Override
    public void execute() {
        RSItem potion = null;
        if (Variables.get().getAccountSettings().getPotion().isAllIdsFound()) {
            potion = InventoryWrapper.findFirst(Variables.get().getAccountSettings().getPotion().getPotionIDs());
        } else {
            potion = InventoryWrapper.findFirst(Variables.get().getAccountSettings().getPotion().getPotionNames());
        }
        if (potion == null)
            return;
        Clicking.click(Variables.get().getAccountSettings().getPotion().getPotionAction(), potion);
        Timing.waitCondition(Conditions.levelChanged(Variables.get().getAccountSettings().getTrainingSkill(),
                Variables.get().getAccountSettings().getTrainingSkill().getCurrentLevel()), General.random(3000, 5000));
    }
}
