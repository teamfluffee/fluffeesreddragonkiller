package scripts.fluffeesRedDragonKiller.nodes.processNodes.combat;

import org.tribot.api.Clicking;
import org.tribot.api.General;
import org.tribot.api.Timing;
import org.tribot.api2007.GroundItems;
import org.tribot.api2007.Player;
import org.tribot.api2007.WebWalking;
import org.tribot.api2007.types.RSGroundItem;
import org.tribot.api2007.types.RSItemDefinition;
import org.tribot.api2007.types.RSTile;
import scripts.fluffeesRedDragonKiller.data.Variables;
import scripts.fluffeesapi.scripting.antiban.AntiBanSingleton;
import scripts.fluffeesapi.scripting.frameworks.decisionTree.decisionTreeFramework.ProcessNode;
import scripts.fluffeesapi.utilities.Conditions;

import java.util.function.BooleanSupplier;

public class Loot extends ProcessNode {
    @Override
    public String getStatus() {
        return "Looting";
    }

    @Override
    public void execute() {
        RSTile lootTile = new RSTile(Variables.get().getCurrentDragonTile().getX() - 2, Variables.get().getCurrentDragonTile().getY() - 2, Variables.get().getCurrentDragonTile().getPlane());
        RSGroundItem[] loot = GroundItems.getAt(lootTile);
        if (loot.length < 1) {
            Variables.get().setCurrentDragonTile(null);
            return;
        }
        if (!Player.getPosition().equals(lootTile)) {
            WebWalking.walkTo(lootTile);
            Timing.waitCondition(Conditions.nearTile(3, lootTile), General.random(3000, 5000));
        }
        for (int i = 0; i < loot.length && !AntiBanSingleton.get().shouldEat(); i++) {
            Variables.get().setLootGained(false);
            RSItemDefinition lootDefinition = loot[i].getDefinition();
            if (lootDefinition == null)
                continue;
            if (Clicking.click("Take " + lootDefinition.getName(), loot[i]))
                Timing.waitCondition(new BooleanSupplier() {
                    @Override
                    public boolean getAsBoolean() {
                        General.sleep(200, 400);
                        return Variables.get().isLootGained();
                    }
                }, General.random(3000, 5000));
        }

    }
}
