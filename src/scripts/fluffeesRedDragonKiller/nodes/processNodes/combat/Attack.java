package scripts.fluffeesRedDragonKiller.nodes.processNodes.combat;

import org.tribot.api.General;
import org.tribot.api2007.NPCs;
import org.tribot.api2007.Player;
import org.tribot.api2007.ext.Filters;
import org.tribot.api2007.types.RSNPC;
import scripts.fluffeesRedDragonKiller.data.Constants;
import scripts.fluffeesRedDragonKiller.data.Variables;
import scripts.fluffeesapi.scripting.antiban.AntiBanSingleton;
import scripts.fluffeesapi.scripting.antiban.AntibanUtilities;
import scripts.fluffeesapi.scripting.frameworks.decisionTree.decisionTreeFramework.ProcessNode;
import scripts.fluffeesapi.utilities.Skilling;

public class Attack extends ProcessNode {

    @Override
    public String getStatus() {
        return "Attacking Red Dragon";
    }

    @Override
    public void execute() {
        RSNPC monster;
        if (Variables.get().getRedDragons().isAllIdsFound())
            monster = AntiBanSingleton.get().selectNextTarget(NPCs.find(Filters.NPCs.inArea(Constants.ATTACKABLE_AREA).combine(Filters.NPCs.idEquals(Variables.get().getRedDragons().getInteractableIDs()), false)));
        else
            monster = AntiBanSingleton.get().selectNextTarget(NPCs.find(Filters.NPCs.inArea(Constants.ATTACKABLE_AREA).combine(Filters.NPCs.nameEquals(Variables.get().getRedDragons().getName()), false)));
        if (monster == null) {
            return;
            //Wait for monsters to come around or world hop
        } else
            Variables.get().getRedDragons().addInteractableID(monster.getID());
        if (Skilling.rangeMonsterSafeSpotForAntiban(monster)) {
            AntiBanSingleton.get().generateSupportingTrackerInfo(Variables.get().getAccountSettings().getAttackingABC2Sleep(), true);
            long startTime = System.currentTimeMillis();
            while (Player.getRSPlayer().getInteractingIndex() != -1 && Constants.RED_DRAGON_SAFESPOT.equals(Player.getPosition()) && !shouldDrinkPotion()) {
                Variables.get().setCurrentDragonTile(monster.getPosition());
                AntiBanSingleton.get().resolveTimedActions();
                General.sleep(200, 400);
            }
            if (monster.getHealthPercent() >= 0.1)
                Variables.get().setCurrentDragonTile(null);
            if (!Constants.RED_DRAGON_SAFESPOT.equals(Player.getPosition()))
                return;
            Variables.get().getAccountSettings().incrementAttackingABC2Counter();
            Variables.get().getAccountSettings().setAttackingABC2Sleep(AntibanUtilities.addWaitTime((int) (System.currentTimeMillis() - startTime), Variables.get().getAccountSettings().getAttackingABC2Counter(), Variables.get().getAccountSettings().getAttackingABC2Sleep()));
            AntiBanSingleton.get().setLastReactionTime(AntiBanSingleton.get().generateReactionTime(Variables.get().getAccountSettings().getAttackingABC2Sleep(), true));
            AntiBanSingleton.get().generateSupportingTrackerInfo(Variables.get().getAccountSettings().getAttackingABC2Sleep(), true);
            AntiBanSingleton.get().sleepReactionTime();
        }
    }

    public static boolean shouldDrinkPotion() {
        if (!Variables.get().getAccountSettings().isUsePotions())
            return false;
        return Variables.get().getAccountSettings().getTrainingSkill().getCurrentLevel() == Variables.get().getAccountSettings().getTrainingSkill().getActualLevel();
    }
}
