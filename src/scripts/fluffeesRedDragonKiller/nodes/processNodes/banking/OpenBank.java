package scripts.fluffeesRedDragonKiller.nodes.processNodes.banking;

import org.tribot.api.General;
import org.tribot.api.Timing;
import scripts.fluffeesapi.client.clientextensions.Banking;
import scripts.fluffeesapi.scripting.frameworks.decisionTree.decisionTreeFramework.ProcessNode;
import scripts.fluffeesapi.utilities.Conditions;

public class OpenBank extends ProcessNode {
    @Override
    public String getStatus() {
        return "Opening Bank";
    }

    @Override
    public void execute() {
        Banking.openBank();
        Timing.waitCondition(Conditions.bankOpened(), General.random(3000, 5000));
    }


}
