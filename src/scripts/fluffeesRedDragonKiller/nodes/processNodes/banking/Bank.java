package scripts.fluffeesRedDragonKiller.nodes.processNodes.banking;

import org.tribot.api.General;
import org.tribot.api.Timing;
import org.tribot.api2007.Equipment;
import org.tribot.api2007.types.RSItem;
import org.tribot.api2007.types.RSItemDefinition;
import scripts.fluffeesRedDragonKiller.data.Variables;
import scripts.fluffeesapi.client.clientextensions.Banking;
import scripts.fluffeesapi.client.clientextensions.Inventory;
import scripts.fluffeesapi.data.interactables.InteractableEquipment;
import scripts.fluffeesapi.data.interactables.InteractableItem;
import scripts.fluffeesapi.data.interactables.InteractableRune;
import scripts.fluffeesapi.scripting.frameworks.modulardecisiontree.nodes.ProcessNode;
import scripts.fluffeesapi.utilities.Conditions;

public class Bank extends ProcessNode {
    @Override
    public String getStatus() {
        return "Banking";
    }

    @Override
    public void execute() {
        if (Variables.get().getBankfailCounter() >= 3) {
            Variables.get().setStopScript(true);
            return;
        }
        depositLoot();
        if (Equipment.getItems().length != Variables.get().getAccountSettings().getNumberOfEquipment())
            if (!withdrawEquipment())
                return;
        if (Variables.get().getAccountSettings().isUseRanged()) {
            RSItem arrow = Equipment.SLOTS.ARROW.getItem();
            if (arrow == null || arrow.getStack() < Variables.get().getAccountSettings().getAmmo().getQuantity())
                if (!withdrawAmmo())
                    return;
        } else {
            for (int i = 0; i < Variables.get().getAccountSettings().getSpell().getRunesRequired().length; i++) {
                if (!withdrawInteractableItem(Variables.get().getAccountSettings().getSpell().getRunesRequired()[i],
                        Variables.get().getAccountSettings().getSpell().getRunesRequired()[i].getQuantity() * Variables.get().getAccountSettings().getCastsPerRun()))
                    return;
            }
        }
        if (!withdrawInteractableItem(Variables.get().getAccountSettings().getFood()))
            return;
        if (!withdrawInteractableItem(Variables.get().getAccountSettings().getAxe()))
            return;
        if (!withdrawInteractableItem(Variables.get().getCoins()))
            return;
        if (Variables.get().getAccountSettings().isUsePotions())
            if (!withdrawInteractableItem(Variables.get().getAccountSettings().getPotion().getWithdrawPotion()))
                return;
        Banking.close();
        Variables.get().resetBankfailCounter();
        Variables.get().setEndingReason("Unknown reason.");
        if (shouldGrabIDs())
            grabIDs();
    }

    private void depositLoot() {
        if (Variables.get().getAccountSettings().getAxe().isAllIdsFound() && Variables.get().getAccountSettings().getFood().isAllIdsFound() && Variables.get().getCoins().isAllIdsFound()) {
            int foodCount = org.tribot.api2007.Inventory.getCount(Variables.get().getAccountSettings().getFood().getId());
            if (foodCount > Variables.get().getAccountSettings().getFood().getQuantity())
                Banking.deposit(foodCount - Variables.get().getAccountSettings().getFood().getQuantity(), Variables.get().getAccountSettings().getFood().getId());
            if (Variables.get().getAccountSettings().isUsePotions()) {
                if (Variables.get().getAccountSettings().isUseRanged())
                    Banking.depositAllExcept(Variables.get().getAccountSettings().getAxe().getId(), Variables.get().getAccountSettings().getFood().getId(), Variables.get().getCoins().getId(), Variables.get().getAccountSettings().getAmmo().getId(), Variables.get().getAccountSettings().getPotion().getWithdrawPotion().getId());
                else
                    Banking.depositAllExcept(Variables.get().getAccountSettings().getAxe().getId(), Variables.get().getAccountSettings().getFood().getId(), Variables.get().getCoins().getId(), Variables.get().getAccountSettings().getSpell().getFirstRuneID(), Variables.get().getAccountSettings().getSpell().getSecondRuneID(), Variables.get().getAccountSettings().getSpell().getThirdRuneID(), Variables.get().getAccountSettings().getPotion().getWithdrawPotion().getId());
            } else {
                if (Variables.get().getAccountSettings().isUseRanged())
                    Banking.depositAllExcept(Variables.get().getAccountSettings().getAxe().getId(), Variables.get().getAccountSettings().getFood().getId(), Variables.get().getCoins().getId(), Variables.get().getAccountSettings().getAmmo().getId());
                else
                    Banking.depositAllExcept(Variables.get().getAccountSettings().getAxe().getId(), Variables.get().getAccountSettings().getFood().getId(), Variables.get().getCoins().getId(), Variables.get().getAccountSettings().getSpell().getFirstRuneID(), Variables.get().getAccountSettings().getSpell().getSecondRuneID(), Variables.get().getAccountSettings().getSpell().getThirdRuneID());

            }
        } else {
            int foodCount = org.tribot.api2007.Inventory.getCount(Variables.get().getAccountSettings().getFood().getName());
            if (foodCount > Variables.get().getAccountSettings().getFood().getQuantity())
                Banking.deposit(foodCount - Variables.get().getAccountSettings().getFood().getQuantity(), Variables.get().getAccountSettings().getFood().getName());
            if (Variables.get().getAccountSettings().isUsePotions()) {
                if (Variables.get().getAccountSettings().isUseRanged())
                    Banking.depositAllExcept(Variables.get().getAccountSettings().getAxe().getName(), Variables.get().getAccountSettings().getFood().getName(), Variables.get().getCoins().getName(), Variables.get().getAccountSettings().getAmmo().getName(), Variables.get().getAccountSettings().getPotion().getWithdrawPotion().getName());
                else
                    Banking.depositAllExcept(Variables.get().getAccountSettings().getAxe().getName(), Variables.get().getAccountSettings().getFood().getName(), Variables.get().getCoins().getName(), Variables.get().getAccountSettings().getSpell().getFirstRuneName(), Variables.get().getAccountSettings().getSpell().getSecondRuneName(), Variables.get().getAccountSettings().getSpell().getThirdRuneName(), Variables.get().getAccountSettings().getPotion().getWithdrawPotion().getName());
            } else {
                if (Variables.get().getAccountSettings().isUseRanged())
                    Banking.depositAllExcept(Variables.get().getAccountSettings().getAxe().getName(), Variables.get().getAccountSettings().getFood().getName(), Variables.get().getCoins().getName(), Variables.get().getAccountSettings().getAmmo().getName());
                else
                    Banking.depositAllExcept(Variables.get().getAccountSettings().getAxe().getName(), Variables.get().getAccountSettings().getFood().getName(), Variables.get().getCoins().getName(), Variables.get().getAccountSettings().getSpell().getFirstRuneName(), Variables.get().getAccountSettings().getSpell().getSecondRuneName(), Variables.get().getAccountSettings().getSpell().getThirdRuneName());
            }
        }

    }

    private boolean withdrawEquipment() {
        for (InteractableEquipment currentEquipment : Variables.get().getAccountSettings().getEquipment()) {
            if (currentEquipment.getName() == null)
                continue;
            if (currentEquipment.isAllIdsFound()) {
                if (Equipment.getItem(currentEquipment.getSlot()).getID() == currentEquipment.getId())
                    continue;
                else if (Banking.find(currentEquipment.getId()).length < 1) {
                    Variables.get().incrementBankfailCounter();
                    Variables.get().setEndingReason("Failure to withdraw: " + currentEquipment.getName() + ".");
                    return false;
                } else {
                    Banking.withdraw(currentEquipment.getQuantity(), currentEquipment.getId());
                    Timing.waitCondition(Conditions.inventoryCountChanged(org.tribot.api2007.Inventory.getAll().length), General.random(3000, 5000));
                }
            } else {
                RSItem slotItem = Equipment.getItem(currentEquipment.getSlot());
                if (slotItem != null) {
                    RSItemDefinition rsItemDefinition = slotItem.getDefinition();
                    if (rsItemDefinition != null && rsItemDefinition.getName().equals(currentEquipment.getName()))
                        continue;
                } else if (Banking.find(currentEquipment.getName()).length < 1) {
                    Variables.get().incrementBankfailCounter();
                    Variables.get().setEndingReason("Failure to withdraw " + currentEquipment.getName() + ".");
                    return false;
                } else {
                    Banking.withdraw(currentEquipment.getQuantity(), currentEquipment.getName());
                    Timing.waitCondition(Conditions.inventoryCountChanged(org.tribot.api2007.Inventory.getAll().length), General.random(3000, 5000));
                }
            }
            General.sleep(200, 400);
        }
        return true;
    }

    private boolean withdrawAmmo() {
        RSItem arrow = Equipment.SLOTS.ARROW.getItem();
        int equippedAmmo = 0;
        if (arrow != null)
            equippedAmmo = arrow.getStack();
        if (Variables.get().getAccountSettings().getAmmo().isAllIdsFound()) {
            if (Banking.find(Variables.get().getAccountSettings().getAmmo().getId()).length < 1) {
                Variables.get().incrementBankfailCounter();
                Variables.get().setEndingReason("Failure to withdraw " + Variables.get().getAccountSettings().getAmmo().getName() + ".");
                return false;
            } else {
                Banking.withdraw(Variables.get().getAccountSettings().getAmmo().getQuantity() - equippedAmmo - org.tribot.api2007.Inventory.getCount(Variables.get().getAccountSettings().getAmmo().getId()), Variables.get().getAccountSettings().getAmmo().getId());
                Timing.waitCondition(Conditions.inventoryCountChanged(org.tribot.api2007.Inventory.getAll().length), General.random(3000, 5000));
            }
        } else {
            if (Banking.find(Variables.get().getAccountSettings().getAmmo().getName()).length < 1) {
                Variables.get().incrementBankfailCounter();
                Variables.get().setEndingReason("Failure to withdraw " + Variables.get().getAccountSettings().getAmmo().getName() + ".");
                return false;
            } else {
                Banking.withdraw(Variables.get().getAccountSettings().getAmmo().getQuantity() - equippedAmmo - org.tribot.api2007.Inventory.getCount(Variables.get().getAccountSettings().getAmmo().getName()), Variables.get().getAccountSettings().getAmmo().getName());
                Timing.waitCondition(Conditions.inventoryCountChanged(org.tribot.api2007.Inventory.getAll().length), General.random(3000, 5000));
            }
        }
        return true;
    }

    private boolean withdrawInteractableItem(InteractableItem item) {
        return withdrawInteractableItem(item, item.getQuantity());
    }

    private boolean withdrawInteractableItem(InteractableItem item, int quantity) {
        int amountToWithdraw = 0;
        int inventoryCount = Inventory.getAll().length;
        if (item.isAllIdsFound())
            amountToWithdraw = quantity - org.tribot.api2007.Inventory.getCount(item.getId());
        else
            amountToWithdraw = quantity - org.tribot.api2007.Inventory.getCount(item.getName());
        if (amountToWithdraw == 0)
            return true;
        else if (amountToWithdraw < 0) {
            if (item.isAllIdsFound()) {
                if (Banking.deposit((amountToWithdraw * -1), item.getId()))
                    Timing.waitCondition(Conditions.inventoryCountChanged(inventoryCount), General.random(3000, 5000));
            } else {
                if (Banking.deposit((amountToWithdraw * -1), item.getName()))
                    Timing.waitCondition(Conditions.inventoryCountChanged(inventoryCount), General.random(3000, 5000));
            }
            return true;
        }
        if (item.isAllIdsFound()) {
            if (!Banking.withdraw(amountToWithdraw, item.getId())) {
                Variables.get().incrementBankfailCounter();
                Variables.get().setEndingReason("Failure to withdraw " + item.getName() + ".");
                return false;
            }
            Timing.waitCondition(Conditions.inventoryCountChanged(inventoryCount), General.random(3000, 5000));
        } else {
            if (!Banking.withdraw(amountToWithdraw, item.getName())) {
                Variables.get().incrementBankfailCounter();
                Variables.get().setEndingReason("Failure to withdraw " + item.getName() + ".");
                return false;
            }
            Timing.waitCondition(Conditions.inventoryCountChanged(inventoryCount), General.random(3000, 5000));
        }
        return true;
    }

    public boolean shouldGrabIDs() {
        boolean valid = false;
        for (InteractableEquipment equipment : Variables.get().getAccountSettings().getEquipment())
            valid = !equipment.isAllIdsFound();
        if (Variables.get().getAccountSettings().isUseRanged())
            valid = !Variables.get().getAccountSettings().getAmmo().isAllIdsFound();
        else
            for (InteractableRune rune : Variables.get().getAccountSettings().getSpell().getRunesRequired())
                valid = !rune.isAllIdsFound();
        if (Variables.get().getAccountSettings().isUsePotions())
            valid = Variables.get().getAccountSettings().getPotion().isAllIdsFound();
        return valid || !Variables.get().getAccountSettings().getFood().isAllIdsFound() || !Variables.get().getAccountSettings().getAxe().isAllIdsFound();
    }

    public void grabIDs() {
        RSItem item = null;
        for (int i = 0; i < Variables.get().getAccountSettings().getEquipment().length; i++) {
            if (Variables.get().getAccountSettings().getEquipment()[i].isAllIdsFound())
                continue;
            item = Variables.get().getAccountSettings().getEquipment()[i].getSlot().getItem();
            if (item != null)
                Variables.get().getAccountSettings().getEquipment()[i].addInteractableID(item.getID());
        }
        if (Variables.get().getAccountSettings().isUseRanged()) {
            if (!Variables.get().getAccountSettings().getAmmo().isAllIdsFound()) {
                item = Variables.get().getAccountSettings().getAmmo().getSlot().getItem();
                if (item != null)
                    Variables.get().getAccountSettings().getAmmo().addInteractableID(item.getID());
            }
        } else {
            for (int i = 0; i < Variables.get().getAccountSettings().getSpell().getRunesRequired().length; i++) {
                if (Variables.get().getAccountSettings().getSpell().getRunesRequired()[i].isAllIdsFound())
                    continue;
                item = Inventory.findFirst(Variables.get().getAccountSettings().getSpell().getRunesRequired()[i].getName());
                if (item != null)
                    Variables.get().getAccountSettings().getSpell().getRunesRequired()[i].addInteractableID(item.getID());
            }
        }
        if (!Variables.get().getAccountSettings().getAxe().isAllIdsFound()) {
            item = Inventory.findFirst(Variables.get().getAccountSettings().getAxe().getName());
            if (item != null)
                Variables.get().getAccountSettings().getAxe().addInteractableID(item.getID());
        }
        if (!Variables.get().getAccountSettings().getFood().isAllIdsFound()) {
            item = Inventory.findFirst(Variables.get().getAccountSettings().getFood().getName());
            if (item != null)
                Variables.get().getAccountSettings().getFood().addInteractableID(item.getID());
        }
    }
}
