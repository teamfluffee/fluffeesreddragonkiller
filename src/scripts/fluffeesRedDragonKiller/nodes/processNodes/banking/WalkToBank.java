package scripts.fluffeesRedDragonKiller.nodes.processNodes.banking;

import org.tribot.api2007.WebWalking;
import scripts.fluffeesRedDragonKiller.data.Constants;
import scripts.fluffeesapi.scripting.frameworks.decisionTree.decisionTreeFramework.ProcessNode;

public class WalkToBank extends ProcessNode {
    @Override
    public String getStatus() {
        return "Walking to Bank";
    }

    @Override
    public void execute() {
        WebWalking.walkTo(Constants.FIGHT_PITS_BANK);
    }
}
