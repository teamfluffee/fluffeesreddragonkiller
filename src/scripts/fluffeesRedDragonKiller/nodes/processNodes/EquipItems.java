package scripts.fluffeesRedDragonKiller.nodes.processNodes;

import org.tribot.api.Clicking;
import org.tribot.api.General;
import org.tribot.api.Timing;
import org.tribot.api2007.Inventory;
import org.tribot.api2007.types.RSItem;
import scripts.fluffeesRedDragonKiller.data.Variables;
import scripts.fluffeesapi.client.wrappers.clientWrappers.InventoryWrapper;
import scripts.fluffeesapi.scripting.frameworks.decisionTree.decisionTreeFramework.ProcessNode;
import scripts.fluffeesapi.utilities.Conditions;

public class EquipItems extends ProcessNode {
    @Override
    public String getStatus() {
        return "Equipping Items";
    }

    @Override
    public void execute() {
        RSItem[] temp = InventoryWrapper.find(Variables.get().getAccountSettings().getAmmo());
        if (temp.length > 0) {
            Clicking.click(Variables.get().getAccountSettings().getAmmo().getAction(), temp[0]);
        }
        temp = InventoryWrapper.find(Variables.get().getAccountSettings().getEquipment());
        for (RSItem current : temp) {
            int inventoryCount = Inventory.getAll().length;
            Clicking.click(new String[]{"Wear", "Wield"}, current);
            Timing.waitCondition(Conditions.inventoryCountChanged(inventoryCount), General.random(3000, 5000));
        }
    }
}
