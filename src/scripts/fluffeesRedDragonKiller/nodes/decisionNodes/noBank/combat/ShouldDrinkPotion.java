package scripts.fluffeesRedDragonKiller.nodes.decisionNodes.noBank.combat;

import scripts.fluffeesRedDragonKiller.data.Variables;
import scripts.fluffeesapi.scripting.frameworks.decisionTree.decisionTreeFramework.DecisionNode;

public class ShouldDrinkPotion extends DecisionNode {
    @Override
    public boolean isValid() {
        if (!Variables.get().getAccountSettings().isUsePotions())
            return false;
         return Variables.get().getAccountSettings().getTrainingSkill().getCurrentLevel() == Variables.get().getAccountSettings().getTrainingSkill().getActualLevel();
    }
}
