package scripts.fluffeesRedDragonKiller.nodes.decisionNodes.noBank.combat;

import org.tribot.api2007.Player;
import scripts.fluffeesapi.scripting.frameworks.decisionTree.decisionTreeFramework.DecisionNode;

public class InCombat extends DecisionNode {

    @Override
    public boolean isValid() {
        return Player.getRSPlayer().getInteractingIndex() != -1 || Player.getRSPlayer().isInCombat();
    }

}