package scripts.fluffeesRedDragonKiller.nodes.decisionNodes.noBank.combat;

import scripts.fluffeesRedDragonKiller.data.Variables;
import scripts.fluffeesapi.scripting.frameworks.decisionTree.decisionTreeFramework.DecisionNode;

public class IsLooting extends DecisionNode {

    @Override
    public boolean isValid() {
        return Variables.get().getCurrentDragonTile() != null;
    }

}