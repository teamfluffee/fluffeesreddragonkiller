package scripts.fluffeesRedDragonKiller.nodes.decisionNodes.noBank.combat;

import scripts.fluffeesRedDragonKiller.data.Variables;
import scripts.fluffeesapi.game.magic.Autocast;
import scripts.fluffeesapi.scripting.frameworks.decisionTree.decisionTreeFramework.DecisionNode;

public class NeedEnableAutocast extends DecisionNode {
    @Override
    public boolean isValid() {
        return !Variables.get().getAccountSettings().isUseRanged() && !Autocast.isAutocastEnabled(Variables.get().getAccountSettings().getSpell());
    }
}
