package scripts.fluffeesRedDragonKiller.nodes.decisionNodes.noBank.combat;

import org.tribot.api2007.Inventory;
import scripts.fluffeesRedDragonKiller.data.Variables;
import scripts.fluffeesapi.client.clientextensions.Banking;
import scripts.fluffeesapi.scripting.antiban.AntiBanSingleton;
import scripts.fluffeesapi.scripting.frameworks.decisionTree.decisionTreeFramework.DecisionNode;

public class ShouldEat extends DecisionNode {

    @Override
    public boolean isValid() {
        if (Variables.get().getAccountSettings().getFood().isAllIdsFound())
            return Inventory.find(Variables.get().getAccountSettings().getFood().getInteractableIDs()).length > 0 && AntiBanSingleton.get().shouldEat() && !Banking.isBankScreenOpen();
        else
            return Inventory.find(Variables.get().getAccountSettings().getFood().getName()).length > 0 && AntiBanSingleton.get().shouldEat() && !Banking.isBankScreenOpen();
    }

}