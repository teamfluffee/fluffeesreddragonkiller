package scripts.fluffeesRedDragonKiller.nodes.decisionNodes.noBank.combat;

import org.tribot.api2007.Player;
import scripts.fluffeesRedDragonKiller.data.Constants;
import scripts.fluffeesapi.scripting.frameworks.decisionTree.decisionTreeFramework.DecisionNode;

public class InSafespot extends DecisionNode {

    @Override
    public boolean isValid() {
        return Constants.RED_DRAGON_SAFESPOT.equals(Player.getPosition());
    }
}