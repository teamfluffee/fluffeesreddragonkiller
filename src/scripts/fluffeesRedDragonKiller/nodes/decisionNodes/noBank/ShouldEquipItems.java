package scripts.fluffeesRedDragonKiller.nodes.decisionNodes.noBank;

import scripts.fluffeesRedDragonKiller.data.Variables;
import scripts.fluffeesapi.client.wrappers.clientWrappers.InventoryWrapper;
import scripts.fluffeesapi.scripting.frameworks.decisionTree.decisionTreeFramework.DecisionNode;

public class ShouldEquipItems extends DecisionNode {
    @Override
    public boolean isValid() {
        boolean valid = false;
        if (Variables.get().getAccountSettings().isUseRanged())
            valid = InventoryWrapper.inventoryContains(Variables.get().getAccountSettings().getAmmo());
        return valid || InventoryWrapper.inventoryContains(Variables.get().getAccountSettings().getEquipment());
    }
}
