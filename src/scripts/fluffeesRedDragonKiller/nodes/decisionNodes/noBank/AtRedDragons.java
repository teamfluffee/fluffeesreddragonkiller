package scripts.fluffeesRedDragonKiller.nodes.decisionNodes.noBank;

import org.tribot.api2007.Player;
import scripts.fluffeesRedDragonKiller.data.Constants;
import scripts.fluffeesapi.scripting.frameworks.decisionTree.decisionTreeFramework.DecisionNode;

public class AtRedDragons extends DecisionNode {

    @Override
    public boolean isValid() {
        return Constants.BRIMHAVEN_RED_DRAGONS_AREA.contains(Player.getPosition());
    }

}