package scripts.fluffeesRedDragonKiller.nodes.decisionNodes.noBank.walkToDragons;

import org.tribot.api2007.Player;
import scripts.fluffeesRedDragonKiller.data.Constants;
import scripts.fluffeesapi.scripting.frameworks.decisionTree.decisionTreeFramework.DecisionNode;

public class InFourthRoom extends DecisionNode {
    @Override
    public boolean isValid() {
        return Constants.BRIMHAVEN_DUNGEON_FOURTH_ROOM.contains(Player.getPosition());
    }
}

