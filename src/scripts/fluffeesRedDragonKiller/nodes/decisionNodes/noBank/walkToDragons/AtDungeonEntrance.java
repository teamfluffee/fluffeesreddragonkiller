package scripts.fluffeesRedDragonKiller.nodes.decisionNodes.noBank.walkToDragons;

import org.tribot.api2007.Objects;
import scripts.fluffeesRedDragonKiller.data.Variables;
import scripts.fluffeesapi.scripting.frameworks.decisionTree.decisionTreeFramework.DecisionNode;
import scripts.fluffeesapi.scripting.types.interactables.Interactable;

public class AtDungeonEntrance extends DecisionNode {

    @Override
    public boolean isValid() {
        if (Variables.get().getDungeonEntrance().getId() == Interactable.DEFAULT_ID)
            return Objects.findNearest(7, Variables.get().getDungeonEntrance().getName()).length > 0;
        else
            return Objects.findNearest(7, Variables.get().getDungeonEntrance().getId()).length > 0;
    }
}