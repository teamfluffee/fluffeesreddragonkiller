package scripts.fluffeesRedDragonKiller.nodes.decisionNodes.noBank.walkToDragons;

import org.tribot.api2007.types.RSVarBit;
import scripts.fluffeesRedDragonKiller.data.Constants;
import scripts.fluffeesapi.scripting.frameworks.decisionTree.decisionTreeFramework.DecisionNode;

public class PaidToEnterDungeon extends DecisionNode {

    @Override
    public boolean isValid() {
        return RSVarBit.get(Constants.PAID_TO_ENTER_VARBIT).getValue() ==  1;
    }
}
