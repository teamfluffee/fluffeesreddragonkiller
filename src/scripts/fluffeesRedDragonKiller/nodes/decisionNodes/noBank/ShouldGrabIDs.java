package scripts.fluffeesRedDragonKiller.nodes.decisionNodes.noBank;

import scripts.fluffeesRedDragonKiller.data.Variables;
import scripts.fluffeesapi.scripting.frameworks.decisionTree.decisionTreeFramework.DecisionNode;
import scripts.fluffeesapi.scripting.types.interactables.InteractableEquipment;
import scripts.fluffeesapi.scripting.types.interactables.InteractableRune;

public class ShouldGrabIDs extends DecisionNode {
    @Override
    public boolean isValid() {
        boolean valid = false;
        for (InteractableEquipment equipment : Variables.get().getAccountSettings().getEquipment())
            valid = !equipment.isAllIdsFound() ? true : valid;
        if (Variables.get().getAccountSettings().isUseRanged())
            valid = !Variables.get().getAccountSettings().getAmmo().isAllIdsFound() ? true : valid;
        else
            for (InteractableRune rune : Variables.get().getAccountSettings().getSpell().getRunesRequired())
                valid = !rune.isAllIdsFound() ? true : valid;
        if (Variables.get().getAccountSettings().isUsePotions())
            valid = Variables.get().getAccountSettings().getPotion().isAllIdsFound() ? true : valid;
        return valid || !Variables.get().getAccountSettings().getFood().isAllIdsFound() || !Variables.get().getAccountSettings().getAxe().isAllIdsFound();
    }
}
