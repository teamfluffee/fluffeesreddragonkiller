package scripts.fluffeesRedDragonKiller.nodes.decisionNodes.yesBank;

import scripts.fluffeesapi.client.clientextensions.Banking;
import scripts.fluffeesapi.scripting.frameworks.decisionTree.decisionTreeFramework.DecisionNode;

public class BankOpen extends DecisionNode {
    @Override
    public boolean isValid() {
        return Banking.isBankScreenOpen();
    }
}
