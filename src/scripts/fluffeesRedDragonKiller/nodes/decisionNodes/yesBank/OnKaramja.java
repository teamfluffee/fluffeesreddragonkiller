package scripts.fluffeesRedDragonKiller.nodes.decisionNodes.yesBank;

import org.tribot.api2007.Player;
import org.tribot.api2007.types.RSArea;
import org.tribot.api2007.types.RSTile;
import scripts.fluffeesapi.scripting.frameworks.decisionTree.decisionTreeFramework.DecisionNode;

public class OnKaramja extends DecisionNode {

    //Karamja upmost coord: 3247
    //Karamja downmost coord: 3137
    //Karamja rightmost coord: 2930
    //Karamja leftmost coord: 2704

    private final RSArea KARAMJA = new RSArea(new RSTile(2704, 3247, 0), new RSTile(2930, 3137, 0));

    @Override
    public boolean isValid() {
        return KARAMJA.contains(Player.getPosition());
    }
}