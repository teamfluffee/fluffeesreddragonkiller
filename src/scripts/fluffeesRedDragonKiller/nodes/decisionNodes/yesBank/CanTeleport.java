package scripts.fluffeesRedDragonKiller.nodes.decisionNodes.yesBank;

import org.tribot.api2007.Player;
import scripts.fluffeesRedDragonKiller.data.Variables;
import scripts.fluffeesapi.scripting.frameworks.decisionTree.decisionTreeFramework.DecisionNode;

public class CanTeleport extends DecisionNode {
    @Override
    public boolean isValid() {
        return !Player.getRSPlayer().isInCombat() && Variables.get().canTeleport();
    }
}
