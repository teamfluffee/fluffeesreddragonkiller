package scripts.fluffeesRedDragonKiller.nodes.decisionNodes.yesBank;

import scripts.fluffeesapi.game.banking.BankingUtilities;
import scripts.fluffeesapi.scripting.frameworks.decisionTree.decisionTreeFramework.DecisionNode;

public class BankLoaded extends DecisionNode {
    @Override
    public boolean isValid() {
        return BankingUtilities.isBankItemsLoaded();
    }
}
