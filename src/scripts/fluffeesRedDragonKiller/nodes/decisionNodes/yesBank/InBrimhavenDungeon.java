package scripts.fluffeesRedDragonKiller.nodes.decisionNodes.yesBank;

import org.tribot.api2007.Player;
import scripts.fluffeesRedDragonKiller.data.Constants;
import scripts.fluffeesapi.scripting.frameworks.decisionTree.decisionTreeFramework.DecisionNode;

public class InBrimhavenDungeon extends DecisionNode {
    @Override
    public boolean isValid() {
        return Constants.BRIMHAVEN_DUNGEON.contains(Player.getPosition());
    }
}
