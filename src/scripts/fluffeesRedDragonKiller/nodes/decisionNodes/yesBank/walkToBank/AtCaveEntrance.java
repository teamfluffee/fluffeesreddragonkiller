package scripts.fluffeesRedDragonKiller.nodes.decisionNodes.yesBank.walkToBank;

import org.tribot.api2007.Objects;
import org.tribot.api2007.ext.Filters;
import scripts.fluffeesapi.scripting.frameworks.decisionTree.decisionTreeFramework.DecisionNode;

public class AtCaveEntrance extends DecisionNode {

    @Override
    public boolean isValid() {
        return Objects.findNearest(7, Filters.Objects.actionsContains("Climb-down").combine(Filters.Objects.nameEquals("Rocks"), false)).length > 0;
    }
}