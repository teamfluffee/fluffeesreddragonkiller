package scripts.fluffeesRedDragonKiller.nodes.decisionNodes.yesBank.walkToBank;

import org.tribot.api2007.Player;
import org.tribot.api2007.types.RSArea;
import org.tribot.api2007.types.RSTile;
import scripts.fluffeesapi.scripting.frameworks.decisionTree.decisionTreeFramework.DecisionNode;

public class InCave extends DecisionNode {

    //Karamja upmost coord: 9581
    //Karamja downmost coord: 9565
    //Karamja rightmost coord: 2868
    //Karamja leftmost coord: 2846

    private final RSArea CAVE = new RSArea(new RSTile(2846, 9581, 0), new RSTile(2868, 9565, 0));

    @Override
    public boolean isValid() {
        return CAVE.contains(Player.getPosition());
    }
}