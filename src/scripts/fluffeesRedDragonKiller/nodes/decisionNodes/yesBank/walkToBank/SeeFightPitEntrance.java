package scripts.fluffeesRedDragonKiller.nodes.decisionNodes.yesBank.walkToBank;

import org.tribot.api2007.Objects;
import scripts.fluffeesapi.scripting.frameworks.decisionTree.decisionTreeFramework.DecisionNode;

public class SeeFightPitEntrance extends DecisionNode {

    @Override
    public boolean isValid() {
        return Objects.findNearest(15, "Cave entrance").length > 0;
    }
}
