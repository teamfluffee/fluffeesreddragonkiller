package scripts.fluffeesRedDragonKiller.nodes.decisionNodes.yesBank.walkToBank;

import org.tribot.api2007.types.RSVarBit;
import scripts.fluffeesRedDragonKiller.data.Constants;
import scripts.fluffeesapi.scripting.frameworks.decisionTree.decisionTreeFramework.DecisionNode;

public class InFightPits extends DecisionNode {

    @Override
    public boolean isValid() {
        return RSVarBit.get(Constants.FIGHT_PITS_VARBIT).getValue() == 1;
    }
}