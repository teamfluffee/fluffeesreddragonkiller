package scripts.fluffeesRedDragonKiller.nodes.decisionNodes;

import org.tribot.api2007.Equipment;
import org.tribot.api2007.Inventory;
import org.tribot.api2007.Player;
import org.tribot.api2007.types.RSItem;
import org.tribot.api2007.types.RSVarBit;
import scripts.fluffeesRedDragonKiller.data.Constants;
import scripts.fluffeesRedDragonKiller.data.Variables;
import scripts.fluffeesapi.client.wrappers.clientWrappers.InventoryWrapper;
import scripts.fluffeesapi.scripting.frameworks.decisionTree.decisionTreeFramework.DecisionNode;
import scripts.fluffeesapi.scripting.types.interactables.InteractableRune;

public class ShouldBank extends DecisionNode {

    @Override
    public boolean isValid() {
        int inventoryEquipmentCount = InventoryWrapper.getCount(Variables.get().getAccountSettings().getEquipment());
        int equippedCount = Equipment.getItems().length;
        boolean valid = false;
        if (Variables.get().getAccountSettings().isUseRanged()) {
            RSItem ammo = Equipment.getItem(Variables.get().getAccountSettings().getAmmo().getSlot());
            valid = (ammo == null || ammo.getStack() == 0) && InventoryWrapper.getCount(Variables.get().getAccountSettings().getAmmo()) < 1
                    || Equipment.getItem(Variables.get().getAccountSettings().getAmmo().getSlot()) == null;
        } else {
            for (InteractableRune rune : Variables.get().getAccountSettings().getSpell().getRunesRequired())
                valid = InventoryWrapper.getCount(rune) == 0;
        }
        if (Variables.get().getAccountSettings().isUsePotions()) {
            if (Variables.get().getAccountSettings().getPotion().isAllIdsFound())
                valid = InventoryWrapper.getCount(Variables.get().getAccountSettings().getPotion().getPotionIDs()) <= 0;
            else
                valid = InventoryWrapper.getCount(Variables.get().getAccountSettings().getPotion().getPotionNames()) <= 0;
        }
        return valid || Inventory.isFull() || InventoryWrapper.getCount(Variables.get().getAccountSettings().getAxe()) < 1 || Inventory.getCount(Variables.get().getAccountSettings().getFood().getName()) == 0 ||
                (equippedCount < Variables.get().getAccountSettings().getEquipment().length && inventoryEquipmentCount < Variables.get().getAccountSettings().getEquipment().length && (inventoryEquipmentCount + equippedCount) < Variables.get().getAccountSettings().getEquipment().length)
                || (!Constants.BRIMHAVEN_DUNGEON.contains(Player.getPosition()) && RSVarBit.get(Constants.PAID_TO_ENTER_VARBIT).getValue() != 1 && Inventory.getCount("Coins") < 875);
    }

}
